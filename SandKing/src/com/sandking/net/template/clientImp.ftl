package ${packageName};
/**
 * ${remark}
 */
 
<#list clientImports as imports>
import ${imports};
</#list>
 
public abstract class ${dclassName} {

	// 逻辑分发
	public void disp(SK_ChannelImp skc, Map<Object,Object> map) throws Exception {
        if(skc == null) return;
        String cmd  = SK_Map.getString("c", map);
        Map<Object, Object> p  = SK_Map.getMap("p", map);
        switch (cmd) {
        <#list methods as method>
		case "${method.methodName}": // ${method.remark}
			${method.methodName}(skc,p);
			break;
		</#list>
		default:
			break;
		}
    }

	<#list methods as method>
	
	/** ${method.remark} */
	private void ${method.methodName}(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
		<#list method.parameterTypes as parameterType>
		<#if parameterType=="int">
		${parameterType} ${method.parameterNames[parameterType_index]} = SK_Map.getInt("${method.parameterNames[parameterType_index]}",map);
	    <#elseif parameterType=="long">
	    ${parameterType} ${method.parameterNames[parameterType_index]} = SK_Map.getLong("${method.parameterNames[parameterType_index]}",map);
	    <#elseif parameterType=="short">
	    ${parameterType} ${method.parameterNames[parameterType_index]} = SK_Map.getShort("${method.parameterNames[parameterType_index]}",map);
	    <#elseif parameterType=="float">
	    ${parameterType} ${method.parameterNames[parameterType_index]} = SK_Map.getFloat("${method.parameterNames[parameterType_index]}",map);
	    <#elseif parameterType=="double">
	    ${parameterType} ${method.parameterNames[parameterType_index]} = SK_Map.getDouble("${method.parameterNames[parameterType_index]}",map);
	    <#elseif parameterType=="String">
	    ${parameterType} ${method.parameterNames[parameterType_index]} = SK_Map.getString("${method.parameterNames[parameterType_index]}",map);
	    </#if>
		</#list>
		
		// 返回
		<#list method.returnTypes as returnType>
		${returnType} ${method.returnNames[returnType_index]} = null;
		</#list>
		//执行逻辑实现
		${method.methodName}(skc,${method.methodParameter});
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		<#list method.returnTypes as returnType>
		p.put("${method.returnNames[returnType_index]}",${method.returnNames[returnType_index]}.toMap());
		</#list>
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	</#list>


	// 需要实现的接口
	<#list methods as method>
	
	/** ${method.remark} */
	public abstract void ${method.methodName}(SK_ChannelImp skc,${method.methodParameterName});
	</#list>
	
}
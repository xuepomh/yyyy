package com.sandking.net.pojo;

import java.util.List;

import com.sandking.tools.SK_Plus;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午4:59:44
 * @Description ：Please describe this document
 */
public class NetMethod {
	private String remark;
	private String methodName;
	private List<String> parameterTypes;
	private List<String> parameterNames;
	private List<String> returnTypes;
	private List<String> returnNames;
	// 方法参数
	private String methodParameter;
	// 方法参数名称
	private String methodParameterName;
	// 返回类型
	private String returnObject;

	public String getMethodParameter() {
		if (methodParameter == null) {
			SK_Plus plus = SK_Plus.newSK_Plus();
			for (int i = 0; i < parameterTypes.size(); i++) {
				if (i > 0) {
					plus.a(",");
				}
				plus.as(parameterNames.get(i));
			}
			if (parameterTypes.size() > 0 && returnTypes.size() > 0) {
				plus.a(",");
			}
			for (int i = 0; i < returnTypes.size(); i++) {
				if (i > 0) {
					plus.a(",");
				}
				plus.as(returnNames.get(i));
			}
			methodParameter = plus.e();
		}
		return methodParameter;
	}

	public void setMethodParameter(String methodParameter) {
		this.methodParameter = methodParameter;
	}

	public String getMethodParameterName() {
		if (methodParameterName == null) {
			SK_Plus plus = SK_Plus.newSK_Plus();
			for (int i = 0; i < parameterTypes.size(); i++) {
				if (i > 0) {
					plus.a(",");
				}
				plus.as(parameterTypes.get(i), " ", parameterNames.get(i));
			}
			if (parameterTypes.size() > 0 && returnTypes.size() > 0) {
				plus.a(",");
			}
			for (int i = 0; i < returnTypes.size(); i++) {
				if (i > 0) {
					plus.a(",");
				}
				plus.as(returnTypes.get(i), " ", returnNames.get(i));
			}
			methodParameterName = plus.e();
		}
		return methodParameterName;
	}

	public void setMethodParameterName(String methodParameterName) {
		this.methodParameterName = methodParameterName;
	}

	public NetMethod(String remark, String methodName,
			List<String> parameterTypes, List<String> parameterNames,
			List<String> returnTypes, List<String> returnNames,String returnObject) {
		super();
		this.remark = remark;
		this.methodName = methodName;
		this.parameterTypes = parameterTypes;
		this.parameterNames = parameterNames;
		this.returnTypes = returnTypes;
		this.returnNames = returnNames;
		this.returnObject = returnObject;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<String> getParameterTypes() {
		return parameterTypes;
	}

	public void setParameterTypes(List<String> parameterTypes) {
		this.parameterTypes = parameterTypes;
	}

	public List<String> getParameterNames() {
		return parameterNames;
	}

	public void setParameterNames(List<String> parameterNames) {
		this.parameterNames = parameterNames;
	}

	public List<String> getReturnTypes() {
		return returnTypes;
	}

	public void setReturnTypes(List<String> returnTypes) {
		this.returnTypes = returnTypes;
	}

	public List<String> getReturnNames() {
		return returnNames;
	}

	public void setReturnNames(List<String> returnNames) {
		this.returnNames = returnNames;
	}

	public String getReturnObject() {
		return returnObject;
	}

	public void setReturnObject(String returnObject) {
		this.returnObject = returnObject;
	}

}

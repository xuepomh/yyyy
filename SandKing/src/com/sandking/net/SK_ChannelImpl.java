package com.sandking.net;

import io.netty.channel.ChannelHandlerContext;

import java.util.Map;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.sandking.io.SK_OutputStream;

/**
 * @UserName : SandKing
 * @DataTime : 2014年6月26日 下午8:41:35
 * @Description ：Please describe this document
 */
public class SK_ChannelImpl implements SK_ChannelImp {
	private ChannelHandlerContext channelHandlerContext;
	private Map<Object, Object> params;

	public SK_ChannelImpl(ChannelHandlerContext channelHandlerContext,
			Map<Object, Object> params) {
		super();
		this.channelHandlerContext = channelHandlerContext;
		this.params = params;
	}

	public ChannelHandlerContext getChannelHandlerContext() {
		return channelHandlerContext;
	}

	public void setChannelHandlerContext(
			ChannelHandlerContext channelHandlerContext) {
		this.channelHandlerContext = channelHandlerContext;
	}

	public Map<Object, Object> getParams() {
		return params;
	}

	public void setParams(Map<Object, Object> params) {
		this.params = params;
	}

	@Override
	public void send(Map<Object, Object> map) throws Exception {
		try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			SK_OutputStream.writeObject(os, map);
			send(os.toByteArray());
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void send(byte[] buf) throws Exception {
		if (channelHandlerContext != null && !channelHandlerContext.isRemoved()) {
			channelHandlerContext.writeAndFlush(buf);
		}
	}
}

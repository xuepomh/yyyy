package com.sandking.net.test;

import org.apache.commons.io.FileUtils;

import com.bowlong.io.FileEx;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.Modifier;
import javassist.NotFoundException;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;

/**
 * @UserName : SandKing
 * @DataTime : 2014年2月26日 下午12:41:24
 * @Description ：网络接口生成器
 */
public class Test {

	public int id;
	public static int seg;
	// @SK_Method(remark = "注释")
	private final int ggg = 1;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) throws NotFoundException,
			ClassNotFoundException {
		Class clazz = Test.class;
		ClassPool pool = ClassPool.getDefault();
		CtClass cc = pool.getCtClass(clazz.getName());
		CtMethod cm = cc.getDeclaredMethod("test");
		// 取得参数类型
		CtClass[] parameterTypes = cm.getParameterTypes();
		for (CtClass ctClass : parameterTypes) {
			System.out.println(ctClass.getSimpleName());
		}
		// 取得参数名称
		MethodInfo methodInfo = cm.getMethodInfo();
		CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
		LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute
				.getAttribute(LocalVariableAttribute.tag);
		if (attr == null) {
			// exception
		}
		String[] paramNames = new String[cm.getParameterTypes().length];
		int pos = Modifier.isStatic(cm.getModifiers()) ? 0 : 1;
		for (int i = 0; i < paramNames.length; i++)
			paramNames[i] = attr.variableName(i + pos);
		for (int i = 0; i < paramNames.length; i++) {
			System.out.println(paramNames[i]);
		}
//
//		// 取得返回类型
//		CtClass returnType = cm.getReturnType();
//		System.out.println(returnType.getSimpleName());
//
//		System.out.println(cm.getModifiers());
//		Class clazz = Test.class;
//		ClassPool pool = ClassPool.getDefault();
//		CtClass cc = pool.get(clazz.getName());
//		System.out.println(cc.getPackageName());
//		CtField[] fields = cc.getDeclaredFields();
//		for (CtField ctField : fields) {
//			CtClass type = ctField.getType();
//			System.out.println(type.getSimpleName());
//			System.out.println(ctField.getName());
//			System.out.println(ctField.getConstantValue());
////			SK_Method sk_m = (SK_Method) ctField.getAnnotation;
////			if (sk_m != null) {
////				System.out.println(sk_m.remark());
////			}
//			System.out.println("-----------------------");
//		}
	}

	public static void test(String info, String aaa) {

	}
}
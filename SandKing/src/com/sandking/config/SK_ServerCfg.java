package com.sandking.config;


/**
 * @UserName : SandKing
 * @DataTime : 2013年11月30日 下午5:17:19
 * @Description ：Please describe this document
 */
public class SK_ServerCfg {
	private String serverName;
	private String version;
	private int tcpPort;
	private int httpPort;
	private int stopPort;

	public SK_ServerCfg(String serverName, String version, int tcpPort,
			int httpPort, int stopPort) {
		super();
		this.serverName = serverName;
		this.version = version;
		this.tcpPort = tcpPort;
		this.httpPort = httpPort;
		this.stopPort = stopPort;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getTcpPort() {
		return tcpPort;
	}

	public void setTcpPort(int tcpPort) {
		this.tcpPort = tcpPort;
	}

	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

	public int getStopPort() {
		return stopPort;
	}

	public void setStopPort(int stopPort) {
		this.stopPort = stopPort;
	}
}

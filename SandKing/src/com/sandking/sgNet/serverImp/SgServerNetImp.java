package com.sandking.sgNet.serverImp;
/**
 * 登录
 */
 
import com.sandking.tools.SK_Map;
import java.util.Map;
import com.sandking.sgNet.netObject.User;
import com.sandking.net.SK_ChannelImp;
import com.sandking.sgNet.netObject.ReturnStatus;
import java.util.HashMap;
 
public abstract class SgServerNetImp {

	// 逻辑分发
	public void disp(SK_ChannelImp skc, Map<Object,Object> map) throws Exception {
        if(skc == null) return;
        String cmd  = SK_Map.getString("c", map);
        Map<Object, Object> p  = SK_Map.getMap("p", map);
        switch (cmd) {
		case "d": // 登录
			d(skc,p);
			break;
		case "a": // 登录
			a(skc,p);
			break;
		case "e": // 移动
			e(skc,p);
			break;
		case "b": // 移动
			b(skc,p);
			break;
		case "c": // 登录
			c(skc,p);
			break;
		case "f": // 登录
			f(skc,p);
			break;
		default:
			break;
		}
    }

	
	/** 登录 */
	private void d(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
	    String zh = SK_Map.getString("zh",map);
	    String mm = SK_Map.getString("mm",map);
		
		// 返回
		User user = null;
		//执行逻辑实现
		d(skc,zh,mm,user);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		p.put("user",user.toMap());
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** 登录 */
	private void a(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
	    String zh = SK_Map.getString("zh",map);
	    String mm = SK_Map.getString("mm",map);
		
		// 返回
		User user = null;
		ReturnStatus rs = new ReturnStatus();
		//执行逻辑实现
		a(skc,zh,mm,user,rs);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("s", rs.toMap());
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		p.put("user",user.toMap());
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** 移动 */
	private void e(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
	    String zh = SK_Map.getString("zh",map);
	    String mm = SK_Map.getString("mm",map);
		
		// 返回
		//执行逻辑实现
		e(skc,zh,mm);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** 移动 */
	private void b(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
	    String zh = SK_Map.getString("zh",map);
	    String mm = SK_Map.getString("mm",map);
		
		// 返回
		ReturnStatus rs = new ReturnStatus();
		//执行逻辑实现
		b(skc,zh,mm,rs);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("s", rs.toMap());
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** 登录 */
	private void c(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
		
		// 返回
		User user = null;
		ReturnStatus rs = new ReturnStatus();
		//执行逻辑实现
		c(skc,user,rs);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("s", rs.toMap());
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		p.put("user",user.toMap());
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/** 登录 */
	private void f(SK_ChannelImp skc,Map<Object, Object> map){
		// 参数
		
		// 返回
		User user = null;
		//执行逻辑实现
		f(skc,user);
		
		Map<Object,Object> ret = new HashMap<Object, Object>();
		ret.put("c", "move");
		Map<Object,Object> p = new HashMap<Object, Object>();
		ret.put("p", p);
		p.put("user",user.toMap());
		
		try {
			skc.send(ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


	// 需要实现的接口
	
	/** 登录 */
	public abstract void d(SK_ChannelImp skc,String zh,String mm,User user);
	
	/** 登录 */
	public abstract ReturnStatus a(SK_ChannelImp skc,String zh,String mm,User user,ReturnStatus rs);
	
	/** 移动 */
	public abstract void e(SK_ChannelImp skc,String zh,String mm);
	
	/** 移动 */
	public abstract ReturnStatus b(SK_ChannelImp skc,String zh,String mm,ReturnStatus rs);
	
	/** 登录 */
	public abstract ReturnStatus c(SK_ChannelImp skc,User user,ReturnStatus rs);
	
	/** 登录 */
	public abstract void f(SK_ChannelImp skc,User user);
	
}
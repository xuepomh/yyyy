package com.sandking.sgNet.netObject;

import java.util.Map;
import java.util.HashMap;
 

/**
 * Bool类型
 */
public class N_Bool{
	/** 值 */
	private boolean val;
	
	public boolean getVal(){
		return val;
	}
	
	public void setVal(boolean val){
		this.val = val;
	}
	
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("val",val);
        return result;
    }
}
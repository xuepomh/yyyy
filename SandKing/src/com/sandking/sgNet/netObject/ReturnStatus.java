package com.sandking.sgNet.netObject;

import java.util.Map;
import java.util.HashMap;
 

/**
 * 返回状态
 */
public class ReturnStatus{
	/** 编号 */
	private int code;
	/** 消息 */
	private String msg;
	
	public int getCode(){
		return code;
	}
	
	public void setCode(int code){
		this.code = code;
	}
	
	public String getMsg(){
		return msg;
	}
	
	public void setMsg(String msg){
		this.msg = msg;
	}
	
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code",code);
        result.put("msg",msg);
        return result;
    }
}
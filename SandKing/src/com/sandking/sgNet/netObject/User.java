package com.sandking.sgNet.netObject;

import java.util.Map;
import java.util.HashMap;
 

/**
 * 用户类型
 */
public class User{
	/** id */
	private int id;
	/** 用户昵称 */
	private String name;
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id",id);
        result.put("name",name);
        return result;
    }
}
package com.sandking.sgNet.netObject;

import java.util.Map;
import java.util.HashMap;
 

/**
 * string类型
 */
public class N_Str{
	/** 值 */
	private String val;
	
	public String getVal(){
		return val;
	}
	
	public void setVal(String val){
		this.val = val;
	}
	
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("val",val);
        return result;
    }
}
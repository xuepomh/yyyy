package com.sandking.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @UserName : SandKing
 * @DataTime : 2012-9-17 上午11:39:36
 * @Description ：Please describe this document
 */
public class SK_Check {

	/**
	 * 判断是不是合法手机
	 * 
	 * @param handset
	 * @return
	 */
	public static boolean isHandset(String handset) {
		try {
			if (!handset.substring(0, 1).equals("1")) {
				return false;
			}
			if (handset == null || handset.length() != 11) {
				return false;
			}
			String check = "^[0123456789]+$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(handset);
			boolean isMatched = matcher.matches();
			if (isMatched) {
				return true;
			} else {
				return false;
			}
		} catch (RuntimeException e) {
			return false;
		}
	}

	/**
	 * 判断输入的字符串是否符合Email样式.
	 * 
	 * @param str
	 * @return
	 */
	public static final boolean isEmail(final String str) {
		Pattern pattern = Pattern
				.compile("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
		return pattern.matcher(str).matches();
	}

	/**
	 * 判断输入的字符串是否为纯汉字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isChinese(String str) {
		Pattern pattern = Pattern.compile("[\u0391-\uFFE5]+$");
		return pattern.matcher(str).matches();
	}
}

package com.sandking.tools;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class SK_Jedis {
	private static SK_Jedis sk_jedis = null;
	private JedisPool jedisPool;

	private SK_Jedis() {

	}

	public static SK_Jedis getInstance() {
		if (sk_jedis == null) {
			sk_jedis = new SK_Jedis();
		}
		return sk_jedis;
	}

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}

	public Jedis getJedis() {
		return jedisPool.getResource();
	}

	public void returnJedis(Jedis jedis) {
		jedisPool.returnResource(jedis);
	}
}

package com.sandking.metadata;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import opensource.jpinyin.PinyinHelper;

import com.sandking.metadata.decode.SK_SqlTypeDecode;
import com.sandking.tools.SK_String;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月24日 下午6:27:45
 * @Description ：数据库对象
 */
public class SK_Database {
	// 数据库名称
	private String databaseName;
	// 数据库类型
	private String databaseType;
	// 数据库的表集合
	private List<SK_Table> tables;
	private Connection conn;
	private DataSource ds;
	// 类型解码器
	private SK_SqlTypeDecode sqlDecode;
	// 数据源的配置
	private Object config;

	public SK_Database(Connection conn, SK_SqlTypeDecode sqlDecode,
			Object config) {
		this.conn = conn;
		this.databaseName = SK_MetaData.getDatabaseName(this.conn);
		databaseType = SK_MetaData.getDatabaseType(this.conn);
		this.tables = new ArrayList<SK_Table>();
		this.sqlDecode = sqlDecode;
		this.config = config;
	}

	public SK_Database(DataSource ds, SK_SqlTypeDecode sqlDecode, Object config) {
		this.ds = ds;
		try {
			this.conn = ds.getConnection();
			this.databaseName = SK_MetaData.getDatabaseName(this.conn);
			databaseType = SK_MetaData.getDatabaseType(this.conn);
			this.tables = new ArrayList<SK_Table>();
			this.sqlDecode = sqlDecode;
			this.config = config;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	public void setTables(List<SK_Table> tables) {
		this.tables = tables;
	}

	public List<SK_Table> getTables(boolean isConfig) {
		if (tables.isEmpty()) {
			List<Map<String, Object>> names = SK_MetaData.getTables(this.conn,
					this.databaseName);
			SK_Table skTable = null;
			for (Map<String, Object> map : names) {
				// 原名称
				String tableName = map.get("TABLE_NAME").toString();
				List<SK_Column> columns = new ArrayList<SK_Column>();
				List<SK_BindKey> bindKeys = new ArrayList<SK_BindKey>();
				List<SK_Index> indexKeys = new ArrayList<SK_Index>();
				// 拼音名称
				String tableName_ = PinyinHelper.getShortPinyin(tableName);
				// 大写名称
				String d_tableName = SK_String.upperFirst(tableName_);
				// 小写名称
				String x_tableName = SK_String.lowerFirst(tableName_);
				Set<String> beanImports = new HashSet<String>();
				Set<String> daoImports = new HashSet<String>();
				Set<String> cacheImports = new HashSet<String>();
				Set<String> impImports = new HashSet<String>();
				Set<String> jedisImports = new HashSet<String>();
				Set<String> cfgImports = new HashSet<String>();
				List<String> all_objAndGetD_columnNames = new ArrayList<String>();
				skTable = new SK_Table(tableName, d_tableName, x_tableName, "",
						"", "", "", "", "", "", "", "", "", "",
						all_objAndGetD_columnNames, columns, bindKeys,
						indexKeys, this, beanImports, cacheImports,
						jedisImports, daoImports, impImports, cfgImports,
						config.getClass().getSimpleName(), 0, isConfig);
				tables.add(skTable);
			}
		}
		return tables;
	}

	public SK_SqlTypeDecode getSqlDecode() {
		return sqlDecode;
	}

	public void setSqlDecode(SK_SqlTypeDecode sqlDecode) {
		this.sqlDecode = sqlDecode;
	}

	public Object getConfig() {
		return config;
	}

	public void setConfig(Object config) {
		this.config = config;
	}

	public String getDatabaseType() {
		return databaseType;
	}

	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}

}

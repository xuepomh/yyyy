package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.FwqDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.bean.Fwq;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 服务器
 */
public class FwqJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		FwqJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Fwq getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Fwq fwq = null;
		try{
			String key = "Fwq_Object";
			String field = SK_Plus.b("id:",id).e();
			fwq = Fwq.createForJson(jedis.hget(key,field));
			if(fwq == null){
				fwq = FwqDao.getById(id);
				if(fwq!=null){
					loadCache(fwq);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return fwq;
	}
	

	public static void loadCache(Fwq fwq){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(fwq,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Fwq fwq,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Fwq_Object";
		String field = SK_Plus.b("id:",fwq.getId()).e();
		String data = fwq.toJson();
		p.hset(key,field,data);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Fwq> fwqs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Fwq fwq : fwqs){
				loadCache(fwq,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Fwq fwq){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(fwq,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Fwq fwq,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Fwq_Object";
		String field = SK_Plus.b("id:",fwq.getId()).e();
		p.hdel(key,field);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Fwq> fwqs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Fwq fwq : fwqs){
				clearCache(fwq,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Fwq insert(Fwq fwq){
		fwq = FwqDao.insert(fwq);
		if(fwq!=null){
			loadCache(fwq);
		}
    	return fwq;
    }
    
    public static Fwq update(Fwq fwq){
    	fwq = FwqDao.update(fwq);
    	if(fwq!=null){
    		clearCache(fwq);
			loadCache(fwq);
		}
    	return fwq;
    }
    
    public static boolean delete(Fwq fwq){
    	boolean bool = FwqDao.delete(fwq);
    	if(bool){
    		clearCache(fwq);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Fwq> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Fwq> fwqs = new ArrayList<Fwq>();
		try{
			String key = "Fwq_Object";
			List<String> jsons = jedis.hvals(key);
			fwqs = Fwq.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return fwqs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fwq> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Fwq> fwqs = new ArrayList<Fwq>();
		try{
			if(!isLoadAll){
				fwqs = FwqDao.getAll();
				loadCaches(fwqs);
				isLoadAll = true;
			}else{
				String key = "Fwq_Object";
				List<String> jsons = jedis.hvals(key);
				fwqs = Fwq.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return fwqs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fwq> getAllByPage(int page,int size,Integer pageCount){
		List<Fwq> fwqs = getAll();
		fwqs = SK_List.getPage(fwqs, page, size, pageCount);
		return fwqs;
	}
}
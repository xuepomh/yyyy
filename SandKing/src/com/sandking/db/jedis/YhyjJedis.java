package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.bean.Yhyj;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import com.sandking.db.dao.YhyjDao;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户邮件
 */
public class YhyjJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhyjJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyj getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhyj yhyj = null;
		try{
			String key = "Yhyj_Object";
			String field = SK_Plus.b("id:",id).e();
			yhyj = Yhyj.createForJson(jedis.hget(key,field));
			if(yhyj == null){
				yhyj = YhyjDao.getById(id);
				if(yhyj!=null){
					loadCache(yhyj);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhyj;
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<Yhyj> getByFjrid(int fjrid){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		try{
			String key = "Yhyj_Index";
			String field = SK_Plus.b(key,"fjrid:",fjrid).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhyj_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhyjs = Yhyj.createForJson(jsons);
			}else{
				yhyjs = YhyjDao.getByFjrid(fjrid);
				if(yhyjs!=null && !yhyjs.isEmpty()){
					loadCaches(yhyjs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyjs;
	}
	
	
	public static List<Yhyj> getByPageFjrid(int fjrid,int page,int size,Integer pageCount){
		List<Yhyj> yhyjs = getByFjrid(fjrid);
		yhyjs = SK_List.getPage(yhyjs, page, size, pageCount);
		return yhyjs;
	}
	
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Yhyj> getByJsrid(int jsrid){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		try{
			String key = "Yhyj_Index";
			String field = SK_Plus.b(key,"jsrid:",jsrid).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhyj_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhyjs = Yhyj.createForJson(jsons);
			}else{
				yhyjs = YhyjDao.getByJsrid(jsrid);
				if(yhyjs!=null && !yhyjs.isEmpty()){
					loadCaches(yhyjs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyjs;
	}
	
	
	public static List<Yhyj> getByPageJsrid(int jsrid,int page,int size,Integer pageCount){
		List<Yhyj> yhyjs = getByJsrid(jsrid);
		yhyjs = SK_List.getPage(yhyjs, page, size, pageCount);
		return yhyjs;
	}
	

	public static void loadCache(Yhyj yhyj){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhyj,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhyj yhyj,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhyj_Object";
		String field = SK_Plus.b("id:",yhyj.getId()).e();
		String data = yhyj.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhyj.getId());
		key = "Yhyj_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"fjrid:",yhyj.getFjrid()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"jsrid:",yhyj.getJsrid()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhyj> yhyjs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhyj yhyj : yhyjs){
				loadCache(yhyj,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhyj yhyj){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhyj,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhyj yhyj,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhyj_Object";
		String field = SK_Plus.b("id:",yhyj.getId()).e();
		p.hdel(key,field);
		
		key = "Yhyj_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhyj.getId());
		field = SK_Plus.b(key,"fjrid:",yhyj.getFjrid()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"jsrid:",yhyj.getJsrid()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhyj> yhyjs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhyj yhyj : yhyjs){
				clearCache(yhyj,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhyj insert(Yhyj yhyj){
		yhyj = YhyjDao.insert(yhyj);
		if(yhyj!=null){
			loadCache(yhyj);
		}
    	return yhyj;
    }
    
    public static Yhyj update(Yhyj yhyj){
    	yhyj = YhyjDao.update(yhyj);
    	if(yhyj!=null){
    		clearCache(yhyj);
			loadCache(yhyj);
		}
    	return yhyj;
    }
    
    public static boolean delete(Yhyj yhyj){
    	boolean bool = YhyjDao.delete(yhyj);
    	if(bool){
    		clearCache(yhyj);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhyj> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		try{
			String key = "Yhyj_Object";
			List<String> jsons = jedis.hvals(key);
			yhyjs = Yhyj.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyj> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		try{
			if(!isLoadAll){
				yhyjs = YhyjDao.getAll();
				loadCaches(yhyjs);
				isLoadAll = true;
			}else{
				String key = "Yhyj_Object";
				List<String> jsons = jedis.hvals(key);
				yhyjs = Yhyj.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhyjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyj> getAllByPage(int page,int size,Integer pageCount){
		List<Yhyj> yhyjs = getAll();
		yhyjs = SK_List.getPage(yhyjs, page, size, pageCount);
		return yhyjs;
	}
}
package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import com.sandking.db.bean.Ltlx;
import java.util.List;
import com.sandking.db.dao.LtlxDao;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 聊天类型
 */
public class LtlxJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LtlxJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Ltlx getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Ltlx ltlx = null;
		try{
			String key = "Ltlx_Object";
			String field = SK_Plus.b("id:",id).e();
			ltlx = Ltlx.createForJson(jedis.hget(key,field));
			if(ltlx == null){
				ltlx = LtlxDao.getById(id);
				if(ltlx!=null){
					loadCache(ltlx);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return ltlx;
	}
	

	public static void loadCache(Ltlx ltlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(ltlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Ltlx ltlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Ltlx_Object";
		String field = SK_Plus.b("id:",ltlx.getId()).e();
		String data = ltlx.toJson();
		p.hset(key,field,data);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Ltlx> ltlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Ltlx ltlx : ltlxs){
				loadCache(ltlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Ltlx ltlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(ltlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Ltlx ltlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Ltlx_Object";
		String field = SK_Plus.b("id:",ltlx.getId()).e();
		p.hdel(key,field);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Ltlx> ltlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Ltlx ltlx : ltlxs){
				clearCache(ltlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Ltlx insert(Ltlx ltlx){
		ltlx = LtlxDao.insert(ltlx);
		if(ltlx!=null){
			loadCache(ltlx);
		}
    	return ltlx;
    }
    
    public static Ltlx update(Ltlx ltlx){
    	ltlx = LtlxDao.update(ltlx);
    	if(ltlx!=null){
    		clearCache(ltlx);
			loadCache(ltlx);
		}
    	return ltlx;
    }
    
    public static boolean delete(Ltlx ltlx){
    	boolean bool = LtlxDao.delete(ltlx);
    	if(bool){
    		clearCache(ltlx);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Ltlx> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Ltlx> ltlxs = new ArrayList<Ltlx>();
		try{
			String key = "Ltlx_Object";
			List<String> jsons = jedis.hvals(key);
			ltlxs = Ltlx.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return ltlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Ltlx> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Ltlx> ltlxs = new ArrayList<Ltlx>();
		try{
			if(!isLoadAll){
				ltlxs = LtlxDao.getAll();
				loadCaches(ltlxs);
				isLoadAll = true;
			}else{
				String key = "Ltlx_Object";
				List<String> jsons = jedis.hvals(key);
				ltlxs = Ltlx.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return ltlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Ltlx> getAllByPage(int page,int size,Integer pageCount){
		List<Ltlx> ltlxs = getAll();
		ltlxs = SK_List.getPage(ltlxs, page, size, pageCount);
		return ltlxs;
	}
}
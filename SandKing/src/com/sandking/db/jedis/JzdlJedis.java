package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.JzdlDao;
import com.sandking.db.bean.Jzdl;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 建筑队列
 */
public class JzdlJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		JzdlJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Jzdl getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Jzdl jzdl = null;
		try{
			String key = "Jzdl_Object";
			String field = SK_Plus.b("id:",id).e();
			jzdl = Jzdl.createForJson(jedis.hget(key,field));
			if(jzdl == null){
				jzdl = JzdlDao.getById(id);
				if(jzdl!=null){
					loadCache(jzdl);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return jzdl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Jzdl> getByYhjz_id(int yhjz_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Jzdl> jzdls = new ArrayList<Jzdl>();
		try{
			String key = "Jzdl_Index";
			String field = SK_Plus.b(key,"yhjz_id:",yhjz_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Jzdl_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				jzdls = Jzdl.createForJson(jsons);
			}else{
				jzdls = JzdlDao.getByYhjz_id(yhjz_id);
				if(jzdls!=null && !jzdls.isEmpty()){
					loadCaches(jzdls);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return jzdls;
	}
	
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,int page,int size,Integer pageCount){
		List<Jzdl> jzdls = getByYhjz_id(yhjz_id);
		jzdls = SK_List.getPage(jzdls, page, size, pageCount);
		return jzdls;
	}
	

	public static void loadCache(Jzdl jzdl){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(jzdl,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Jzdl jzdl,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Jzdl_Object";
		String field = SK_Plus.b("id:",jzdl.getId()).e();
		String data = jzdl.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(jzdl.getId());
		key = "Jzdl_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yhjz_id:",jzdl.getYhjz_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Jzdl> jzdls){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Jzdl jzdl : jzdls){
				loadCache(jzdl,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Jzdl jzdl){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(jzdl,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Jzdl jzdl,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Jzdl_Object";
		String field = SK_Plus.b("id:",jzdl.getId()).e();
		p.hdel(key,field);
		
		key = "Jzdl_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(jzdl.getId());
		field = SK_Plus.b(key,"yhjz_id:",jzdl.getYhjz_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Jzdl> jzdls){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Jzdl jzdl : jzdls){
				clearCache(jzdl,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Jzdl insert(Jzdl jzdl){
		jzdl = JzdlDao.insert(jzdl);
		if(jzdl!=null){
			loadCache(jzdl);
		}
    	return jzdl;
    }
    
    public static Jzdl update(Jzdl jzdl){
    	jzdl = JzdlDao.update(jzdl);
    	if(jzdl!=null){
    		clearCache(jzdl);
			loadCache(jzdl);
		}
    	return jzdl;
    }
    
    public static boolean delete(Jzdl jzdl){
    	boolean bool = JzdlDao.delete(jzdl);
    	if(bool){
    		clearCache(jzdl);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Jzdl> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Jzdl> jzdls = new ArrayList<Jzdl>();
		try{
			String key = "Jzdl_Object";
			List<String> jsons = jedis.hvals(key);
			jzdls = Jzdl.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return jzdls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Jzdl> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Jzdl> jzdls = new ArrayList<Jzdl>();
		try{
			if(!isLoadAll){
				jzdls = JzdlDao.getAll();
				loadCaches(jzdls);
				isLoadAll = true;
			}else{
				String key = "Jzdl_Object";
				List<String> jsons = jedis.hvals(key);
				jzdls = Jzdl.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return jzdls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Jzdl> getAllByPage(int page,int size,Integer pageCount){
		List<Jzdl> jzdls = getAll();
		jzdls = SK_List.getPage(jzdls, page, size, pageCount);
		return jzdls;
	}
}
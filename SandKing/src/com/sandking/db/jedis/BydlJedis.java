package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.BydlDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.db.bean.Bydl;
import com.sandking.tools.SK_List;
/**
 * 兵营队列
 */
public class BydlJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		BydlJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Bydl getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Bydl bydl = null;
		try{
			String key = "Bydl_Object";
			String field = SK_Plus.b("id:",id).e();
			bydl = Bydl.createForJson(jedis.hget(key,field));
			if(bydl == null){
				bydl = BydlDao.getById(id);
				if(bydl!=null){
					loadCache(bydl);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return bydl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Bydl> getByYhjz_id(int yhjz_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Bydl> bydls = new ArrayList<Bydl>();
		try{
			String key = "Bydl_Index";
			String field = SK_Plus.b(key,"yhjz_id:",yhjz_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Bydl_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				bydls = Bydl.createForJson(jsons);
			}else{
				bydls = BydlDao.getByYhjz_id(yhjz_id);
				if(bydls!=null && !bydls.isEmpty()){
					loadCaches(bydls);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return bydls;
	}
	
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,int page,int size,Integer pageCount){
		List<Bydl> bydls = getByYhjz_id(yhjz_id);
		bydls = SK_List.getPage(bydls, page, size, pageCount);
		return bydls;
	}
	

	public static void loadCache(Bydl bydl){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(bydl,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Bydl bydl,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Bydl_Object";
		String field = SK_Plus.b("id:",bydl.getId()).e();
		String data = bydl.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(bydl.getId());
		key = "Bydl_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yhjz_id:",bydl.getYhjz_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Bydl> bydls){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Bydl bydl : bydls){
				loadCache(bydl,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Bydl bydl){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(bydl,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Bydl bydl,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Bydl_Object";
		String field = SK_Plus.b("id:",bydl.getId()).e();
		p.hdel(key,field);
		
		key = "Bydl_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(bydl.getId());
		field = SK_Plus.b(key,"yhjz_id:",bydl.getYhjz_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Bydl> bydls){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Bydl bydl : bydls){
				clearCache(bydl,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Bydl insert(Bydl bydl){
		bydl = BydlDao.insert(bydl);
		if(bydl!=null){
			loadCache(bydl);
		}
    	return bydl;
    }
    
    public static Bydl update(Bydl bydl){
    	bydl = BydlDao.update(bydl);
    	if(bydl!=null){
    		clearCache(bydl);
			loadCache(bydl);
		}
    	return bydl;
    }
    
    public static boolean delete(Bydl bydl){
    	boolean bool = BydlDao.delete(bydl);
    	if(bool){
    		clearCache(bydl);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Bydl> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Bydl> bydls = new ArrayList<Bydl>();
		try{
			String key = "Bydl_Object";
			List<String> jsons = jedis.hvals(key);
			bydls = Bydl.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return bydls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Bydl> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Bydl> bydls = new ArrayList<Bydl>();
		try{
			if(!isLoadAll){
				bydls = BydlDao.getAll();
				loadCaches(bydls);
				isLoadAll = true;
			}else{
				String key = "Bydl_Object";
				List<String> jsons = jedis.hvals(key);
				bydls = Bydl.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return bydls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Bydl> getAllByPage(int page,int size,Integer pageCount){
		List<Bydl> bydls = getAll();
		bydls = SK_List.getPage(bydls, page, size, pageCount);
		return bydls;
	}
}
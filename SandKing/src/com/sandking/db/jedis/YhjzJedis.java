package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.YhjzDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import com.sandking.db.bean.Yhjz;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户建筑
 */
public class YhjzJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhjzJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhjz getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhjz yhjz = null;
		try{
			String key = "Yhjz_Object";
			String field = SK_Plus.b("id:",id).e();
			yhjz = Yhjz.createForJson(jedis.hget(key,field));
			if(yhjz == null){
				yhjz = YhjzDao.getById(id);
				if(yhjz!=null){
					loadCache(yhjz);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhjz;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhjz> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		try{
			String key = "Yhjz_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhjz_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhjzs = Yhjz.createForJson(jsons);
			}else{
				yhjzs = YhjzDao.getByYh_id(yh_id);
				if(yhjzs!=null && !yhjzs.isEmpty()){
					loadCaches(yhjzs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhjzs;
	}
	
	
	public static List<Yhjz> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhjz> yhjzs = getByYh_id(yh_id);
		yhjzs = SK_List.getPage(yhjzs, page, size, pageCount);
		return yhjzs;
	}
	

	public static void loadCache(Yhjz yhjz){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhjz,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhjz yhjz,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhjz_Object";
		String field = SK_Plus.b("id:",yhjz.getId()).e();
		String data = yhjz.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhjz.getId());
		key = "Yhjz_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhjz.getYh_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhjz> yhjzs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhjz yhjz : yhjzs){
				loadCache(yhjz,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhjz yhjz){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhjz,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhjz yhjz,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhjz_Object";
		String field = SK_Plus.b("id:",yhjz.getId()).e();
		p.hdel(key,field);
		
		key = "Yhjz_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhjz.getId());
		field = SK_Plus.b(key,"yh_id:",yhjz.getYh_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhjz> yhjzs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhjz yhjz : yhjzs){
				clearCache(yhjz,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhjz insert(Yhjz yhjz){
		yhjz = YhjzDao.insert(yhjz);
		if(yhjz!=null){
			loadCache(yhjz);
		}
    	return yhjz;
    }
    
    public static Yhjz update(Yhjz yhjz){
    	yhjz = YhjzDao.update(yhjz);
    	if(yhjz!=null){
    		clearCache(yhjz);
			loadCache(yhjz);
		}
    	return yhjz;
    }
    
    public static boolean delete(Yhjz yhjz){
    	boolean bool = YhjzDao.delete(yhjz);
    	if(bool){
    		clearCache(yhjz);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhjz> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		try{
			String key = "Yhjz_Object";
			List<String> jsons = jedis.hvals(key);
			yhjzs = Yhjz.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhjzs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhjz> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		try{
			if(!isLoadAll){
				yhjzs = YhjzDao.getAll();
				loadCaches(yhjzs);
				isLoadAll = true;
			}else{
				String key = "Yhjz_Object";
				List<String> jsons = jedis.hvals(key);
				yhjzs = Yhjz.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhjzs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhjz> getAllByPage(int page,int size,Integer pageCount){
		List<Yhjz> yhjzs = getAll();
		yhjzs = SK_List.getPage(yhjzs, page, size, pageCount);
		return yhjzs;
	}
}
package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.dao.YjlxDao;
import com.sandking.db.bean.Yjlx;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 邮件类型
 */
public class YjlxJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YjlxJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yjlx getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yjlx yjlx = null;
		try{
			String key = "Yjlx_Object";
			String field = SK_Plus.b("id:",id).e();
			yjlx = Yjlx.createForJson(jedis.hget(key,field));
			if(yjlx == null){
				yjlx = YjlxDao.getById(id);
				if(yjlx!=null){
					loadCache(yjlx);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yjlx;
	}
	

	public static void loadCache(Yjlx yjlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yjlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yjlx yjlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yjlx_Object";
		String field = SK_Plus.b("id:",yjlx.getId()).e();
		String data = yjlx.toJson();
		p.hset(key,field,data);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yjlx> yjlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yjlx yjlx : yjlxs){
				loadCache(yjlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yjlx yjlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yjlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yjlx yjlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yjlx_Object";
		String field = SK_Plus.b("id:",yjlx.getId()).e();
		p.hdel(key,field);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yjlx> yjlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yjlx yjlx : yjlxs){
				clearCache(yjlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yjlx insert(Yjlx yjlx){
		yjlx = YjlxDao.insert(yjlx);
		if(yjlx!=null){
			loadCache(yjlx);
		}
    	return yjlx;
    }
    
    public static Yjlx update(Yjlx yjlx){
    	yjlx = YjlxDao.update(yjlx);
    	if(yjlx!=null){
    		clearCache(yjlx);
			loadCache(yjlx);
		}
    	return yjlx;
    }
    
    public static boolean delete(Yjlx yjlx){
    	boolean bool = YjlxDao.delete(yjlx);
    	if(bool){
    		clearCache(yjlx);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yjlx> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjlx> yjlxs = new ArrayList<Yjlx>();
		try{
			String key = "Yjlx_Object";
			List<String> jsons = jedis.hvals(key);
			yjlxs = Yjlx.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjlx> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjlx> yjlxs = new ArrayList<Yjlx>();
		try{
			if(!isLoadAll){
				yjlxs = YjlxDao.getAll();
				loadCaches(yjlxs);
				isLoadAll = true;
			}else{
				String key = "Yjlx_Object";
				List<String> jsons = jedis.hvals(key);
				yjlxs = Yjlx.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjlx> getAllByPage(int page,int size,Integer pageCount){
		List<Yjlx> yjlxs = getAll();
		yjlxs = SK_List.getPage(yjlxs, page, size, pageCount);
		return yjlxs;
	}
}
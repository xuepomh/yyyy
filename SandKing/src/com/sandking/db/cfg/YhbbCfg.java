package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户背包
 */
public class YhbbCfg {

	public static final String TABLENAME = "用户背包";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhbbCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhbbCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 用户_id */
	private int yh_id;
	
	/** 道具id */
	private int djid;
	
	/** 数量 */
	private String sl;
	
	
	public YhbbCfg() {
		super();
	}
	
	public YhbbCfg(int id, int yh_id, int djid, String sl) {
		super();
		this.id = id;
		this.yh_id = yh_id;
		this.djid = djid;
		this.sl = sl;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	public int getDjid() {
		return djid;
	}
	
	public void setDjid(int djid) {
		this.djid = djid;
	}
	public String getSl() {
		return sl;
	}
	
	public void setSl(String sl) {
		this.sl = sl;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhbbCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhbbCfg> yhbbCfgs = new ArrayList<YhbbCfg>();
		for (Map<String, Object> map : list) {
			yhbbCfgs.add(createForColumnNameMap(map));
		}
		return yhbbCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhbbCfg createForColumnNameMap(Map<String, Object> map){
    	YhbbCfg obj = new YhbbCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
	    obj.djid = SK_Map.getInt("道具id", map);
	    obj.sl = SK_Map.getString("数量", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.yh_id);
	    SK_OutputStream.writeInt(out,this.djid);
	    SK_OutputStream.writeString(out,this.sl);
    }
    
     public static YhbbCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhbbCfg yhbbCfg = new YhbbCfg();
	    yhbbCfg.id = SK_InputStream.readInt(in,null);
	    yhbbCfg.yh_id = SK_InputStream.readInt(in,null);
	    yhbbCfg.djid = SK_InputStream.readInt(in,null);
	    yhbbCfg.sl = SK_InputStream.readString(in,null);
	    return yhbbCfg;
     }
    
    public static List<YhbbCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhbbCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhbbCfg.TABLENAME);
	}
	
	public static List<YhbbCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhbbCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhbbCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,道具id,数量 FROM " + tableName;
		List<YhbbCfg> yhbbCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhbbCfgs = YhbbCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhbbCfgs;
	}
	
	public static List<YhbbCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhbbCfg> yhbbCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhbbCfgs.size());
			for(YhbbCfg yhbbCfg : yhbbCfgs){
				yhbbCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhbbCfg yhbbCfg){
		idCache.put(SK_Plus.b(yhbbCfg.getId()).e(),yhbbCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhbbCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhbbCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhbbCfg.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhbbCfg getById(int id){
		YhbbCfg yhbbCfg = null;
		String key = SK_Plus.b(id).e();
		yhbbCfg = idCache.get(key);
		
		return yhbbCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhbbCfg> getByYh_id(int yh_id){
		List<YhbbCfg> yhbbCfgs = new ArrayList<YhbbCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhbbCfg yhbbCfg = null;
			for (String k : keys) {
				yhbbCfg = getById(Integer.valueOf(k));
				if (yhbbCfg == null) continue;
					yhbbCfgs.add(yhbbCfg);
			}
		}
		return yhbbCfgs;
	}
	
	public static List<YhbbCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhbbCfg> yhbbCfgs = getByYh_id(yh_id);
		yhbbCfgs = SK_List.getPage(yhbbCfgs, page, size, pageCount);
		return yhbbCfgs;
	}
	
	public static List<YhbbCfg> getAll(){
		return new ArrayList<YhbbCfg>(idCache.values());
	}
	
	public static List<YhbbCfg> getAll(int page,int size,Integer pageCount){
		List<YhbbCfg> yhbbCfgs = getAll();
		yhbbCfgs = SK_List.getPage(yhbbCfgs, page, size, pageCount);
		return yhbbCfgs;
	}
}
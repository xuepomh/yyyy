package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 邮件附件
 */
public class YjfjCfg {

	public static final String TABLENAME = "邮件附件";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YjfjCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YjfjCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yjlx_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> yhyj_idCache = SK_Collections.newMap();
	static final Map<String, Set<String>> fjlx_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 附件id */
	private int fjid;
	
	/** 邮件类型_id */
	private int yjlx_id;
	
	/** 用户邮件_id */
	private int yhyj_id;
	
	/** 附件类型_id */
	private int fjlx_id;
	
	/** 附件数量 */
	private int fjsl;
	
	
	public YjfjCfg() {
		super();
	}
	
	public YjfjCfg(int id, int fjid, int yjlx_id, int yhyj_id, int fjlx_id, int fjsl) {
		super();
		this.id = id;
		this.fjid = fjid;
		this.yjlx_id = yjlx_id;
		this.yhyj_id = yhyj_id;
		this.fjlx_id = fjlx_id;
		this.fjsl = fjsl;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getFjid() {
		return fjid;
	}
	
	public void setFjid(int fjid) {
		this.fjid = fjid;
	}
	public int getYjlx_id() {
		return yjlx_id;
	}
	
	public void setYjlx_id(int yjlx_id) {
		this.yjlx_id = yjlx_id;
	}
	public int getYhyj_id() {
		return yhyj_id;
	}
	
	public void setYhyj_id(int yhyj_id) {
		this.yhyj_id = yhyj_id;
	}
	public int getFjlx_id() {
		return fjlx_id;
	}
	
	public void setFjlx_id(int fjlx_id) {
		this.fjlx_id = fjlx_id;
	}
	public int getFjsl() {
		return fjsl;
	}
	
	public void setFjsl(int fjsl) {
		this.fjsl = fjsl;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YjfjCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YjfjCfg> yjfjCfgs = new ArrayList<YjfjCfg>();
		for (Map<String, Object> map : list) {
			yjfjCfgs.add(createForColumnNameMap(map));
		}
		return yjfjCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YjfjCfg createForColumnNameMap(Map<String, Object> map){
    	YjfjCfg obj = new YjfjCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.fjid = SK_Map.getInt("附件id", map);
	    obj.yjlx_id = SK_Map.getInt("邮件类型_id", map);
	    obj.yhyj_id = SK_Map.getInt("用户邮件_id", map);
	    obj.fjlx_id = SK_Map.getInt("附件类型_id", map);
	    obj.fjsl = SK_Map.getInt("附件数量", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.fjid);
	    SK_OutputStream.writeInt(out,this.yjlx_id);
	    SK_OutputStream.writeInt(out,this.yhyj_id);
	    SK_OutputStream.writeInt(out,this.fjlx_id);
	    SK_OutputStream.writeInt(out,this.fjsl);
    }
    
     public static YjfjCfg forStream(ByteArrayInputStream in) throws Exception {
     	YjfjCfg yjfjCfg = new YjfjCfg();
	    yjfjCfg.id = SK_InputStream.readInt(in,null);
	    yjfjCfg.fjid = SK_InputStream.readInt(in,null);
	    yjfjCfg.yjlx_id = SK_InputStream.readInt(in,null);
	    yjfjCfg.yhyj_id = SK_InputStream.readInt(in,null);
	    yjfjCfg.fjlx_id = SK_InputStream.readInt(in,null);
	    yjfjCfg.fjsl = SK_InputStream.readInt(in,null);
	    return yjfjCfg;
     }
    
    public static List<YjfjCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YjfjCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YjfjCfg.TABLENAME);
	}
	
	public static List<YjfjCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YjfjCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YjfjCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,附件id,邮件类型_id,用户邮件_id,附件类型_id,附件数量 FROM " + tableName;
		List<YjfjCfg> yjfjCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yjfjCfgs = YjfjCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjfjCfgs;
	}
	
	public static List<YjfjCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YjfjCfg> yjfjCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yjfjCfgs.size());
			for(YjfjCfg yjfjCfg : yjfjCfgs){
				yjfjCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YjfjCfg yjfjCfg){
		idCache.put(SK_Plus.b(yjfjCfg.getId()).e(),yjfjCfg);
		Set<String> yjlx_idset = yjlx_idCache.get(String.valueOf(SK_Plus.b(yjfjCfg.getYjlx_id()).e()));
		if(yjlx_idset == null){
			yjlx_idset = new HashSet<String>();
		}
		yjlx_idset.add(String.valueOf(yjfjCfg.getId()));
		yjlx_idCache.put(SK_Plus.b(yjfjCfg.getYjlx_id()).e(),yjlx_idset);
		Set<String> yhyj_idset = yhyj_idCache.get(String.valueOf(SK_Plus.b(yjfjCfg.getYhyj_id()).e()));
		if(yhyj_idset == null){
			yhyj_idset = new HashSet<String>();
		}
		yhyj_idset.add(String.valueOf(yjfjCfg.getId()));
		yhyj_idCache.put(SK_Plus.b(yjfjCfg.getYhyj_id()).e(),yhyj_idset);
		Set<String> fjlx_idset = fjlx_idCache.get(String.valueOf(SK_Plus.b(yjfjCfg.getFjlx_id()).e()));
		if(fjlx_idset == null){
			fjlx_idset = new HashSet<String>();
		}
		fjlx_idset.add(String.valueOf(yjfjCfg.getId()));
		fjlx_idCache.put(SK_Plus.b(yjfjCfg.getFjlx_id()).e(),fjlx_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YjfjCfg getById(int id){
		YjfjCfg yjfjCfg = null;
		String key = SK_Plus.b(id).e();
		yjfjCfg = idCache.get(key);
		
		return yjfjCfg;
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<YjfjCfg> getByYjlx_id(int yjlx_id){
		List<YjfjCfg> yjfjCfgs = new ArrayList<YjfjCfg>();
		String key = SK_Plus.b(yjlx_id).e();
		Set<String> keys = yjlx_idCache.get(key);
		if(keys != null){
			YjfjCfg yjfjCfg = null;
			for (String k : keys) {
				yjfjCfg = getById(Integer.valueOf(k));
				if (yjfjCfg == null) continue;
					yjfjCfgs.add(yjfjCfg);
			}
		}
		return yjfjCfgs;
	}
	
	public static List<YjfjCfg> getByPageYjlx_id(int yjlx_id,int page,int size,Integer pageCount){
		List<YjfjCfg> yjfjCfgs = getByYjlx_id(yjlx_id);
		yjfjCfgs = SK_List.getPage(yjfjCfgs, page, size, pageCount);
		return yjfjCfgs;
	}
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<YjfjCfg> getByYhyj_id(int yhyj_id){
		List<YjfjCfg> yjfjCfgs = new ArrayList<YjfjCfg>();
		String key = SK_Plus.b(yhyj_id).e();
		Set<String> keys = yhyj_idCache.get(key);
		if(keys != null){
			YjfjCfg yjfjCfg = null;
			for (String k : keys) {
				yjfjCfg = getById(Integer.valueOf(k));
				if (yjfjCfg == null) continue;
					yjfjCfgs.add(yjfjCfg);
			}
		}
		return yjfjCfgs;
	}
	
	public static List<YjfjCfg> getByPageYhyj_id(int yhyj_id,int page,int size,Integer pageCount){
		List<YjfjCfg> yjfjCfgs = getByYhyj_id(yhyj_id);
		yjfjCfgs = SK_List.getPage(yjfjCfgs, page, size, pageCount);
		return yjfjCfgs;
	}
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<YjfjCfg> getByFjlx_id(int fjlx_id){
		List<YjfjCfg> yjfjCfgs = new ArrayList<YjfjCfg>();
		String key = SK_Plus.b(fjlx_id).e();
		Set<String> keys = fjlx_idCache.get(key);
		if(keys != null){
			YjfjCfg yjfjCfg = null;
			for (String k : keys) {
				yjfjCfg = getById(Integer.valueOf(k));
				if (yjfjCfg == null) continue;
					yjfjCfgs.add(yjfjCfg);
			}
		}
		return yjfjCfgs;
	}
	
	public static List<YjfjCfg> getByPageFjlx_id(int fjlx_id,int page,int size,Integer pageCount){
		List<YjfjCfg> yjfjCfgs = getByFjlx_id(fjlx_id);
		yjfjCfgs = SK_List.getPage(yjfjCfgs, page, size, pageCount);
		return yjfjCfgs;
	}
	
	public static List<YjfjCfg> getAll(){
		return new ArrayList<YjfjCfg>(idCache.values());
	}
	
	public static List<YjfjCfg> getAll(int page,int size,Integer pageCount){
		List<YjfjCfg> yjfjCfgs = getAll();
		yjfjCfgs = SK_List.getPage(yjfjCfgs, page, size, pageCount);
		return yjfjCfgs;
	}
}
package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 服务器
 */
public class FwqCfg {

	public static final String TABLENAME = "服务器";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		FwqCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, FwqCfg> idCache = SK_Collections.newSortedMap();

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	/** 开服时间 */
	private java.util.Date kfsj;
	
	/** 关服时间 */
	private java.util.Date gfsj;
	
	/** serverkey */
	private String serverkey;
	
	/** 顺序 */
	private int sx;
	
	
	public FwqCfg() {
		super();
	}
	
	public FwqCfg(int id, String mc, java.util.Date kfsj, java.util.Date gfsj, String serverkey, int sx) {
		super();
		this.id = id;
		this.mc = mc;
		this.kfsj = kfsj;
		this.gfsj = gfsj;
		this.serverkey = serverkey;
		this.sx = sx;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
	}
	public java.util.Date getKfsj() {
		return kfsj;
	}
	
	public void setKfsj(java.util.Date kfsj) {
		this.kfsj = kfsj;
	}
	public java.util.Date getGfsj() {
		return gfsj;
	}
	
	public void setGfsj(java.util.Date gfsj) {
		this.gfsj = gfsj;
	}
	public String getServerkey() {
		return serverkey;
	}
	
	public void setServerkey(String serverkey) {
		this.serverkey = serverkey;
	}
	public int getSx() {
		return sx;
	}
	
	public void setSx(int sx) {
		this.sx = sx;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<FwqCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<FwqCfg> fwqCfgs = new ArrayList<FwqCfg>();
		for (Map<String, Object> map : list) {
			fwqCfgs.add(createForColumnNameMap(map));
		}
		return fwqCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static FwqCfg createForColumnNameMap(Map<String, Object> map){
    	FwqCfg obj = new FwqCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
	    obj.kfsj = SK_Map.getDate("开服时间", map);
	    obj.gfsj = SK_Map.getDate("关服时间", map);
	    obj.serverkey = SK_Map.getString("serverkey", map);
	    obj.sx = SK_Map.getInt("顺序", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.mc);
	    SK_OutputStream.writeDate(out,this.kfsj);
	    SK_OutputStream.writeDate(out,this.gfsj);
	    SK_OutputStream.writeString(out,this.serverkey);
	    SK_OutputStream.writeInt(out,this.sx);
    }
    
     public static FwqCfg forStream(ByteArrayInputStream in) throws Exception {
     	FwqCfg fwqCfg = new FwqCfg();
	    fwqCfg.id = SK_InputStream.readInt(in,null);
	    fwqCfg.mc = SK_InputStream.readString(in,null);
	    fwqCfg.kfsj = SK_InputStream.readDate(in,null);
	    fwqCfg.gfsj = SK_InputStream.readDate(in,null);
	    fwqCfg.serverkey = SK_InputStream.readString(in,null);
	    fwqCfg.sx = SK_InputStream.readInt(in,null);
	    return fwqCfg;
     }
    
    public static List<FwqCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<FwqCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,FwqCfg.TABLENAME);
	}
	
	public static List<FwqCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<FwqCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<FwqCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,开服时间,关服时间,serverkey,顺序 FROM " + tableName;
		List<FwqCfg> fwqCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			fwqCfgs = FwqCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fwqCfgs;
	}
	
	public static List<FwqCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<FwqCfg> fwqCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, fwqCfgs.size());
			for(FwqCfg fwqCfg : fwqCfgs){
				fwqCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(FwqCfg fwqCfg){
		idCache.put(SK_Plus.b(fwqCfg.getId()).e(),fwqCfg);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static FwqCfg getById(int id){
		FwqCfg fwqCfg = null;
		String key = SK_Plus.b(id).e();
		fwqCfg = idCache.get(key);
		
		return fwqCfg;
	}
	
	
	public static List<FwqCfg> getAll(){
		return new ArrayList<FwqCfg>(idCache.values());
	}
	
	public static List<FwqCfg> getAll(int page,int size,Integer pageCount){
		List<FwqCfg> fwqCfgs = getAll();
		fwqCfgs = SK_List.getPage(fwqCfgs, page, size, pageCount);
		return fwqCfgs;
	}
}
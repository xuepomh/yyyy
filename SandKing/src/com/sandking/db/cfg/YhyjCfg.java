package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户邮件
 */
public class YhyjCfg {

	public static final String TABLENAME = "用户邮件";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhyjCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhyjCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> fjridCache = SK_Collections.newMap();
	static final Map<String, Set<String>> jsridCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 创建时间 */
	private java.util.Date cjsj;
	
	/** 读取时间 */
	private java.util.Date dqsj;
	
	/** 是否读取 */
	private java.util.Date sfdq;
	
	/** 发件人id */
	private int fjrid;
	
	/** 接收人id */
	private int jsrid;
	
	
	public YhyjCfg() {
		super();
	}
	
	public YhyjCfg(int id, java.util.Date cjsj, java.util.Date dqsj, java.util.Date sfdq, int fjrid, int jsrid) {
		super();
		this.id = id;
		this.cjsj = cjsj;
		this.dqsj = dqsj;
		this.sfdq = sfdq;
		this.fjrid = fjrid;
		this.jsrid = jsrid;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public java.util.Date getCjsj() {
		return cjsj;
	}
	
	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}
	public java.util.Date getDqsj() {
		return dqsj;
	}
	
	public void setDqsj(java.util.Date dqsj) {
		this.dqsj = dqsj;
	}
	public java.util.Date getSfdq() {
		return sfdq;
	}
	
	public void setSfdq(java.util.Date sfdq) {
		this.sfdq = sfdq;
	}
	public int getFjrid() {
		return fjrid;
	}
	
	public void setFjrid(int fjrid) {
		this.fjrid = fjrid;
	}
	public int getJsrid() {
		return jsrid;
	}
	
	public void setJsrid(int jsrid) {
		this.jsrid = jsrid;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhyjCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhyjCfg> yhyjCfgs = new ArrayList<YhyjCfg>();
		for (Map<String, Object> map : list) {
			yhyjCfgs.add(createForColumnNameMap(map));
		}
		return yhyjCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhyjCfg createForColumnNameMap(Map<String, Object> map){
    	YhyjCfg obj = new YhyjCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.cjsj = SK_Map.getDate("创建时间", map);
	    obj.dqsj = SK_Map.getDate("读取时间", map);
	    obj.sfdq = SK_Map.getDate("是否读取", map);
	    obj.fjrid = SK_Map.getInt("发件人id", map);
	    obj.jsrid = SK_Map.getInt("接收人id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeDate(out,this.cjsj);
	    SK_OutputStream.writeDate(out,this.dqsj);
	    SK_OutputStream.writeDate(out,this.sfdq);
	    SK_OutputStream.writeInt(out,this.fjrid);
	    SK_OutputStream.writeInt(out,this.jsrid);
    }
    
     public static YhyjCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhyjCfg yhyjCfg = new YhyjCfg();
	    yhyjCfg.id = SK_InputStream.readInt(in,null);
	    yhyjCfg.cjsj = SK_InputStream.readDate(in,null);
	    yhyjCfg.dqsj = SK_InputStream.readDate(in,null);
	    yhyjCfg.sfdq = SK_InputStream.readDate(in,null);
	    yhyjCfg.fjrid = SK_InputStream.readInt(in,null);
	    yhyjCfg.jsrid = SK_InputStream.readInt(in,null);
	    return yhyjCfg;
     }
    
    public static List<YhyjCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhyjCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhyjCfg.TABLENAME);
	}
	
	public static List<YhyjCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhyjCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhyjCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName;
		List<YhyjCfg> yhyjCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyjCfgs = YhyjCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjCfgs;
	}
	
	public static List<YhyjCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhyjCfg> yhyjCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhyjCfgs.size());
			for(YhyjCfg yhyjCfg : yhyjCfgs){
				yhyjCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhyjCfg yhyjCfg){
		idCache.put(SK_Plus.b(yhyjCfg.getId()).e(),yhyjCfg);
		Set<String> fjridset = fjridCache.get(String.valueOf(SK_Plus.b(yhyjCfg.getFjrid()).e()));
		if(fjridset == null){
			fjridset = new HashSet<String>();
		}
		fjridset.add(String.valueOf(yhyjCfg.getId()));
		fjridCache.put(SK_Plus.b(yhyjCfg.getFjrid()).e(),fjridset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(yhyjCfg.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		jsridset.add(String.valueOf(yhyjCfg.getId()));
		jsridCache.put(SK_Plus.b(yhyjCfg.getJsrid()).e(),jsridset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhyjCfg getById(int id){
		YhyjCfg yhyjCfg = null;
		String key = SK_Plus.b(id).e();
		yhyjCfg = idCache.get(key);
		
		return yhyjCfg;
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<YhyjCfg> getByFjrid(int fjrid){
		List<YhyjCfg> yhyjCfgs = new ArrayList<YhyjCfg>();
		String key = SK_Plus.b(fjrid).e();
		Set<String> keys = fjridCache.get(key);
		if(keys != null){
			YhyjCfg yhyjCfg = null;
			for (String k : keys) {
				yhyjCfg = getById(Integer.valueOf(k));
				if (yhyjCfg == null) continue;
					yhyjCfgs.add(yhyjCfg);
			}
		}
		return yhyjCfgs;
	}
	
	public static List<YhyjCfg> getByPageFjrid(int fjrid,int page,int size,Integer pageCount){
		List<YhyjCfg> yhyjCfgs = getByFjrid(fjrid);
		yhyjCfgs = SK_List.getPage(yhyjCfgs, page, size, pageCount);
		return yhyjCfgs;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<YhyjCfg> getByJsrid(int jsrid){
		List<YhyjCfg> yhyjCfgs = new ArrayList<YhyjCfg>();
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys != null){
			YhyjCfg yhyjCfg = null;
			for (String k : keys) {
				yhyjCfg = getById(Integer.valueOf(k));
				if (yhyjCfg == null) continue;
					yhyjCfgs.add(yhyjCfg);
			}
		}
		return yhyjCfgs;
	}
	
	public static List<YhyjCfg> getByPageJsrid(int jsrid,int page,int size,Integer pageCount){
		List<YhyjCfg> yhyjCfgs = getByJsrid(jsrid);
		yhyjCfgs = SK_List.getPage(yhyjCfgs, page, size, pageCount);
		return yhyjCfgs;
	}
	
	public static List<YhyjCfg> getAll(){
		return new ArrayList<YhyjCfg>(idCache.values());
	}
	
	public static List<YhyjCfg> getAll(int page,int size,Integer pageCount){
		List<YhyjCfg> yhyjCfgs = getAll();
		yhyjCfgs = SK_List.getPage(yhyjCfgs, page, size, pageCount);
		return yhyjCfgs;
	}
}
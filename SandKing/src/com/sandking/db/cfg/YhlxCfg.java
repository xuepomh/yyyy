package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户类型
 */
public class YhlxCfg {

	public static final String TABLENAME = "用户类型";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhlxCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhlxCfg> idCache = SK_Collections.newSortedMap();

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public YhlxCfg() {
		super();
	}
	
	public YhlxCfg(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhlxCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhlxCfg> yhlxCfgs = new ArrayList<YhlxCfg>();
		for (Map<String, Object> map : list) {
			yhlxCfgs.add(createForColumnNameMap(map));
		}
		return yhlxCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhlxCfg createForColumnNameMap(Map<String, Object> map){
    	YhlxCfg obj = new YhlxCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.mc);
    }
    
     public static YhlxCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhlxCfg yhlxCfg = new YhlxCfg();
	    yhlxCfg.id = SK_InputStream.readInt(in,null);
	    yhlxCfg.mc = SK_InputStream.readString(in,null);
	    return yhlxCfg;
     }
    
    public static List<YhlxCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhlxCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhlxCfg.TABLENAME);
	}
	
	public static List<YhlxCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhlxCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhlxCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<YhlxCfg> yhlxCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhlxCfgs = YhlxCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhlxCfgs;
	}
	
	public static List<YhlxCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhlxCfg> yhlxCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhlxCfgs.size());
			for(YhlxCfg yhlxCfg : yhlxCfgs){
				yhlxCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhlxCfg yhlxCfg){
		idCache.put(SK_Plus.b(yhlxCfg.getId()).e(),yhlxCfg);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhlxCfg getById(int id){
		YhlxCfg yhlxCfg = null;
		String key = SK_Plus.b(id).e();
		yhlxCfg = idCache.get(key);
		
		return yhlxCfg;
	}
	
	
	public static List<YhlxCfg> getAll(){
		return new ArrayList<YhlxCfg>(idCache.values());
	}
	
	public static List<YhlxCfg> getAll(int page,int size,Integer pageCount){
		List<YhlxCfg> yhlxCfgs = getAll();
		yhlxCfgs = SK_List.getPage(yhlxCfgs, page, size, pageCount);
		return yhlxCfgs;
	}
}
package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户装饰
 */
public class YhzsCfg {

	public static final String TABLENAME = "用户装饰";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhzsCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, YhzsCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 装饰id */
	private int zsid;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public YhzsCfg() {
		super();
	}
	
	public YhzsCfg(int id, int zsid, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.zsid = zsid;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getZsid() {
		return zsid;
	}
	
	public void setZsid(int zsid) {
		this.zsid = zsid;
	}
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<YhzsCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<YhzsCfg> yhzsCfgs = new ArrayList<YhzsCfg>();
		for (Map<String, Object> map : list) {
			yhzsCfgs.add(createForColumnNameMap(map));
		}
		return yhzsCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static YhzsCfg createForColumnNameMap(Map<String, Object> map){
    	YhzsCfg obj = new YhzsCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.zsid = SK_Map.getInt("装饰id", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.zsid);
	    SK_OutputStream.writeInt(out,this.x);
	    SK_OutputStream.writeInt(out,this.y);
	    SK_OutputStream.writeInt(out,this.yh_id);
    }
    
     public static YhzsCfg forStream(ByteArrayInputStream in) throws Exception {
     	YhzsCfg yhzsCfg = new YhzsCfg();
	    yhzsCfg.id = SK_InputStream.readInt(in,null);
	    yhzsCfg.zsid = SK_InputStream.readInt(in,null);
	    yhzsCfg.x = SK_InputStream.readInt(in,null);
	    yhzsCfg.y = SK_InputStream.readInt(in,null);
	    yhzsCfg.yh_id = SK_InputStream.readInt(in,null);
	    return yhzsCfg;
     }
    
    public static List<YhzsCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<YhzsCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,YhzsCfg.TABLENAME);
	}
	
	public static List<YhzsCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<YhzsCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<YhzsCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,装饰id,x,y,用户_id FROM " + tableName;
		List<YhzsCfg> yhzsCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhzsCfgs = YhzsCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzsCfgs;
	}
	
	public static List<YhzsCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<YhzsCfg> yhzsCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, yhzsCfgs.size());
			for(YhzsCfg yhzsCfg : yhzsCfgs){
				yhzsCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(YhzsCfg yhzsCfg){
		idCache.put(SK_Plus.b(yhzsCfg.getId()).e(),yhzsCfg);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhzsCfg.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhzsCfg.getId()));
		yh_idCache.put(SK_Plus.b(yhzsCfg.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static YhzsCfg getById(int id){
		YhzsCfg yhzsCfg = null;
		String key = SK_Plus.b(id).e();
		yhzsCfg = idCache.get(key);
		
		return yhzsCfg;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<YhzsCfg> getByYh_id(int yh_id){
		List<YhzsCfg> yhzsCfgs = new ArrayList<YhzsCfg>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			YhzsCfg yhzsCfg = null;
			for (String k : keys) {
				yhzsCfg = getById(Integer.valueOf(k));
				if (yhzsCfg == null) continue;
					yhzsCfgs.add(yhzsCfg);
			}
		}
		return yhzsCfgs;
	}
	
	public static List<YhzsCfg> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<YhzsCfg> yhzsCfgs = getByYh_id(yh_id);
		yhzsCfgs = SK_List.getPage(yhzsCfgs, page, size, pageCount);
		return yhzsCfgs;
	}
	
	public static List<YhzsCfg> getAll(){
		return new ArrayList<YhzsCfg>(idCache.values());
	}
	
	public static List<YhzsCfg> getAll(int page,int size,Integer pageCount){
		List<YhzsCfg> yhzsCfgs = getAll();
		yhzsCfgs = SK_List.getPage(yhzsCfgs, page, size, pageCount);
		return yhzsCfgs;
	}
}
package com.sandking.db.dao;

import com.sandking.db.bean.Yhyj;
import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 用户邮件
 */
public class YhyjDao {
	public static Yhyj insert(Yhyj yhyj){
		Connection conn = SK_Config.getConnection();
		return insert(yhyj,conn);
	}
	
	public static Yhyj insert(Yhyj yhyj,Connection conn){
		return insert(yhyj,conn,Yhyj.TABLENAME);
	}
	
	public static Yhyj insert(Yhyj yhyj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhyj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhyj insert(Yhyj yhyj,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhyj,conn,tableName);
	}
	
	public static Yhyj insert(Yhyj yhyj,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,创建时间,读取时间,是否读取,发件人id,接收人id) VALUES (?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhyj.getId(),yhyj.getCjsj(),yhyj.getDqsj(),yhyj.getSfdq(),yhyj.getFjrid(),yhyj.getJsrid());
			if(yhyj.getId()==0){
				yhyj.setId(i);
			}
			return i > 0 ? yhyj : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhyj insert(Yhyj yhyj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhyj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhyj> yhyjs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhyjs,conn);
	}
	
	public static int[] insertBatch(List<Yhyj> yhyjs,Connection conn){
		return insertBatch(yhyjs,conn,Yhyj.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhyj> yhyjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhyjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhyj> yhyjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhyjs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhyj> yhyjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,创建时间,读取时间,是否读取,发件人id,接收人id) VALUES (?,?,?,?,?,?)";
		try {
			int columnSize = 6;
			int size = yhyjs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhyjs.get(i).getId();
				params[i][1] =yhyjs.get(i).getCjsj();
				params[i][2] =yhyjs.get(i).getDqsj();
				params[i][3] =yhyjs.get(i).getSfdq();
				params[i][4] =yhyjs.get(i).getFjrid();
				params[i][5] =yhyjs.get(i).getJsrid();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhyj> yhyjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhyjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhyj update(Yhyj yhyj){
		Connection conn = SK_Config.getConnection();
		return update(yhyj,conn);
	}
	
	public static Yhyj update(Yhyj yhyj,Connection conn){
		return update(yhyj,conn,Yhyj.TABLENAME);
	}
	
	public static Yhyj update(Yhyj yhyj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhyj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhyj update(Yhyj yhyj,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhyj,conn,tableName);
	}
	
	public static Yhyj update(Yhyj yhyj,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhyj.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhyj;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhyj.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhyj : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhyj.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhyj update(Yhyj yhyj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhyj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhyj> yhyjs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhyjs,conn);
	}
	
	public static int[] updateBatch(List<Yhyj> yhyjs,Connection conn){
		return updateBatch(yhyjs,conn,Yhyj.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yhyj> yhyjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhyjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhyj> yhyjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhyjs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yhyj> yhyjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,创建时间 = ?,读取时间 = ?,是否读取 = ?,发件人id = ?,接收人id = ? WHERE id = ?";
		try {
			int columnSize = 6;
			int size = yhyjs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhyjs.get(i).getId();
				params[i][1] =yhyjs.get(i).getCjsj();
				params[i][2] =yhyjs.get(i).getDqsj();
				params[i][3] =yhyjs.get(i).getSfdq();
				params[i][4] =yhyjs.get(i).getFjrid();
				params[i][5] =yhyjs.get(i).getJsrid();
				params[i][columnSize] =yhyjs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yhyj> yhyjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhyjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yhyj yhyj){
		Connection conn = SK_Config.getConnection();
		return delete(yhyj,conn);
	}
	
	public static boolean delete(Yhyj yhyj,Connection conn){
		return delete(yhyj,conn,Yhyj.TABLENAME);
	}
	
	public static boolean delete(Yhyj yhyj,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhyj,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhyj yhyj,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhyj,conn,tableName);
	}
	
	public static boolean delete(Yhyj yhyj,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhyj.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhyj yhyj,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhyj,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhyj> yhyjs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhyjs,conn);
	}
	
	public static boolean deleteBatch(List<Yhyj> yhyjs,Connection conn){
		return deleteBatch(yhyjs,conn,Yhyj.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhyj> yhyjs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhyjs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhyj> yhyjs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhyjs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhyj> yhyjs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhyjs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhyjs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhyj> yhyjs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhyjs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhyj getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhyj getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<Yhyj> getByFjrid(int fjrid){
		Connection conn = SK_Config.getConnection();
		return getByFjrid(fjrid, conn);
	}
	
	public static List<Yhyj> getByFjrid(int fjrid,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByFjrid(fjrid, conn,tableName);
	}
	
	public static List<Yhyj> getByPageFjrid(int fjrid,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageFjrid(fjrid, conn,page,pageSize);
	}
	
	public static List<Yhyj> getByPageFjrid(int fjrid,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageFjrid(fjrid, conn,tableName,page,pageSize);
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Yhyj> getByJsrid(int jsrid){
		Connection conn = SK_Config.getConnection();
		return getByJsrid(jsrid, conn);
	}
	
	public static List<Yhyj> getByJsrid(int jsrid,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByJsrid(jsrid, conn,tableName);
	}
	
	public static List<Yhyj> getByPageJsrid(int jsrid,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageJsrid(jsrid, conn,page,pageSize);
	}
	
	public static List<Yhyj> getByPageJsrid(int jsrid,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageJsrid(jsrid, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyj getById(int id,Connection conn){
		return getById(id,conn,Yhyj.TABLENAME);
	}
	
	public static Yhyj getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yhyj yhyj = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhyj = Yhyj.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyj;
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<Yhyj> getByFjrid(int fjrid,Connection conn){
		return getByFjrid(fjrid,conn,Yhyj.TABLENAME);
	}
	
	public static List<Yhyj> getByFjrid(int fjrid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " WHERE " + "fjrid = ? ORDER BY id ASC";
		List<Yhyj> yhyjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fjrid);
			yhyjs = Yhyj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhyj> getByPageFjrid(int fjrid,Connection conn,int page,int pageSize){
		return getByPageFjrid(fjrid,conn,Yhyj.TABLENAME,page,pageSize);
	}
	
	public static List<Yhyj> getByPageFjrid(int fjrid,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " WHERE " + "fjrid = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhyj> yhyjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fjrid);
			yhyjs = Yhyj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjs;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Yhyj> getByJsrid(int jsrid,Connection conn){
		return getByJsrid(jsrid,conn,Yhyj.TABLENAME);
	}
	
	public static List<Yhyj> getByJsrid(int jsrid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " WHERE " + "jsrid = ? ORDER BY id ASC";
		List<Yhyj> yhyjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), jsrid);
			yhyjs = Yhyj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhyj> getByPageJsrid(int jsrid,Connection conn,int page,int pageSize){
		return getByPageJsrid(jsrid,conn,Yhyj.TABLENAME,page,pageSize);
	}
	
	public static List<Yhyj> getByPageJsrid(int jsrid,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " WHERE " + "jsrid = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhyj> yhyjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), jsrid);
			yhyjs = Yhyj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyj getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhyj getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<Yhyj> getByFjrid(int fjrid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByFjrid(fjrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyj> getByFjrid(int fjrid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByFjrid(fjrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhyj> getByPageFjrid(int fjrid,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageFjrid(fjrid, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyj> getByPageFjrid(int fjrid,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageFjrid(fjrid, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Yhyj> getByJsrid(int jsrid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByJsrid(jsrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyj> getByJsrid(int jsrid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByJsrid(jsrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhyj> getByPageJsrid(int jsrid,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageJsrid(jsrid, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyj> getByPageJsrid(int jsrid,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageJsrid(jsrid, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhyj> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhyj> getAll(Connection conn){
		return getAll(conn,Yhyj.TABLENAME);
	}
	
	public static List<Yhyj> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyj> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhyj> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " ORDER BY id ASC";
		List<Yhyj> yhyjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyjs = Yhyj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjs;
	}
	
	public static List<Yhyj> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhyj> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yhyj> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yhyj.TABLENAME,page,pageSize);
	}
	
	public static List<Yhyj> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyj> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yhyj> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,创建时间,读取时间,是否读取,发件人id,接收人id FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhyj> yhyjs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyjs = Yhyj.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyjs;
	}
	
	public static List<Yhyj> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yhyj.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yhyj.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yhyj.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `创建时间` DATETIME(19) NOT NULL,");	
		plus.a("  `读取时间` DATETIME(19) NOT NULL,");	
		plus.a("  `是否读取` DATETIME(19) NOT NULL,");	
		plus.a("  `发件人id` INT(10) NOT NULL,");	
		plus.a("  `接收人id` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
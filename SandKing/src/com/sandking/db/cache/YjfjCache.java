package com.sandking.db.cache;

import com.sandking.db.dao.YjfjDao;
import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import com.sandking.db.jedis.YjfjJedis;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
import com.sandking.db.bean.Yjfj;
/**
 * 邮件附件
 */
public class YjfjCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YjfjCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yjfj> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yjlx_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> yhyj_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> fjlx_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yjfj> saveDelete = new CopyOnWriteArrayList<Yjfj>();
	
	static final List<Yjfj> saveInsert = new CopyOnWriteArrayList<Yjfj>();
	
	static final List<Yjfj> saveUpdate = new CopyOnWriteArrayList<Yjfj>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yjfj getById(int id){
		Yjfj yjfj = null;
		String key = SK_Plus.b(id).e();
		yjfj = idCache.get(key);
		
		if(yjfj==null){
			//查询数据库
			yjfj = YjfjJedis.getById(id);
			if(yjfj!=null){
				//加入缓存
				loadCache(yjfj);
			}
		}
		return yjfj;
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<Yjfj> getByYjlx_id(int yjlx_id){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		String key = SK_Plus.b(yjlx_id).e();
		Set<String> keys = yjlx_idCache.get(key);
		if(keys != null){
			Yjfj yjfj = null;
			for (String k : keys) {
				yjfj = getById(Integer.valueOf(k));
				if (yjfj == null) continue;
					yjfjs.add(yjfj);
			}
		}else{
			yjfjs = YjfjJedis.getByYjlx_id(yjlx_id);
			if(!yjfjs.isEmpty()){
				loadCaches(yjfjs);
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getByYjlx_id(yjlx_id);
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<Yjfj> getByYhyj_id(int yhyj_id){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		String key = SK_Plus.b(yhyj_id).e();
		Set<String> keys = yhyj_idCache.get(key);
		if(keys != null){
			Yjfj yjfj = null;
			for (String k : keys) {
				yjfj = getById(Integer.valueOf(k));
				if (yjfj == null) continue;
					yjfjs.add(yjfj);
			}
		}else{
			yjfjs = YjfjJedis.getByYhyj_id(yhyj_id);
			if(!yjfjs.isEmpty()){
				loadCaches(yjfjs);
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getByYhyj_id(yhyj_id);
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<Yjfj> getByFjlx_id(int fjlx_id){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		String key = SK_Plus.b(fjlx_id).e();
		Set<String> keys = fjlx_idCache.get(key);
		if(keys != null){
			Yjfj yjfj = null;
			for (String k : keys) {
				yjfj = getById(Integer.valueOf(k));
				if (yjfj == null) continue;
					yjfjs.add(yjfj);
			}
		}else{
			yjfjs = YjfjJedis.getByFjlx_id(fjlx_id);
			if(!yjfjs.isEmpty()){
				loadCaches(yjfjs);
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getByFjlx_id(fjlx_id);
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	
	public static List<Yjfj> getCacheAll(){
		return new ArrayList<Yjfj>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjfj> getAll(){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		if(!isLoadAll){
			yjfjs = YjfjJedis.getAll();
			loadCaches(yjfjs);
			isLoadAll = true;
		}else{
			yjfjs = new ArrayList<Yjfj>(idCache.values());
		}
		return yjfjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjfj> getAllByPage(int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getAll();
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yjfj> yjfjs){
		for(Yjfj yjfj : yjfjs){
			loadCache(yjfj);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yjfj yjfj){
		idCache.put(SK_Plus.b(yjfj.getId()).e(),yjfj);
		Set<String> yjlx_idset = yjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYjlx_id()).e()));
		if(yjlx_idset == null){
			yjlx_idset = new HashSet<String>();
		}
		yjlx_idset.add(String.valueOf(yjfj.getId()));
		yjlx_idCache.put(SK_Plus.b(yjfj.getYjlx_id()).e(),yjlx_idset);
		Set<String> yhyj_idset = yhyj_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYhyj_id()).e()));
		if(yhyj_idset == null){
			yhyj_idset = new HashSet<String>();
		}
		yhyj_idset.add(String.valueOf(yjfj.getId()));
		yhyj_idCache.put(SK_Plus.b(yjfj.getYhyj_id()).e(),yhyj_idset);
		Set<String> fjlx_idset = fjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getFjlx_id()).e()));
		if(fjlx_idset == null){
			fjlx_idset = new HashSet<String>();
		}
		fjlx_idset.add(String.valueOf(yjfj.getId()));
		fjlx_idCache.put(SK_Plus.b(yjfj.getFjlx_id()).e(),fjlx_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yjfj yjfj,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yjfj.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yjfj.getId()).e());
		}
		Set<String> yjlx_idset = yjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYjlx_id()).e()));
		if(yjlx_idset == null){
			yjlx_idset = new HashSet<String>();
		}
		if (yjlx_idset.contains(String.valueOf(yjfj.getId()))) {
			yjlx_idset.remove(String.valueOf(yjfj.getId()));
		}
		yjlx_idCache.put(SK_Plus.b(yjfj.getYjlx_id()).e(),yjlx_idset);
		Set<String> yhyj_idset = yhyj_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYhyj_id()).e()));
		if(yhyj_idset == null){
			yhyj_idset = new HashSet<String>();
		}
		if (yhyj_idset.contains(String.valueOf(yjfj.getId()))) {
			yhyj_idset.remove(String.valueOf(yjfj.getId()));
		}
		yhyj_idCache.put(SK_Plus.b(yjfj.getYhyj_id()).e(),yhyj_idset);
		Set<String> fjlx_idset = fjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getFjlx_id()).e()));
		if(fjlx_idset == null){
			fjlx_idset = new HashSet<String>();
		}
		if (fjlx_idset.contains(String.valueOf(yjfj.getId()))) {
			fjlx_idset.remove(String.valueOf(yjfj.getId()));
		}
		fjlx_idCache.put(SK_Plus.b(yjfj.getFjlx_id()).e(),fjlx_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yjfj);
			break;
		case 2:
			saveRedis(yjfj);
			break;
		case 3:
			saveAll(yjfj);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yjfj> yjfjs,int clearType,int saveType){
		for(Yjfj yjfj : yjfjs){
			clearCache(yjfj,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yjfjs);
			break;
		case 2:
			saveRedis(yjfjs);
			break;
		case 3:
			saveAll(yjfjs);
			break;
		default:
			break;
		}
	}
	
	public static Yjfj insert(Yjfj yjfj){
		return insert(yjfj,false);
    }
    
    public static Yjfj update(Yjfj yjfj){
    	return update(yjfj,false);
    }
    
    public static boolean delete(Yjfj yjfj){
    	return delete(yjfj,false);
    }
    
    private static Yjfj insert(Yjfj yjfj,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yjfj = YjfjJedis.insert(yjfj);
    		LASTID.set(yjfj.getId());
    	}else{
    		int _id = yjfj.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yjfj.getId() > id) {
				LASTID.set(yjfj.getId());
			}
    		//加入定时器
    	}
    	loadCache(yjfj);
    	if(!isFlush){
	    	if (!saveInsert.contains(yjfj)) {
				saveInsert.add(yjfj);
			}
		}
    	return yjfj;
    }
    
    private static Yjfj update(Yjfj yjfj,boolean isFlush){
    	clearCache(yjfj,1,0);
    	loadCache(yjfj);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yjfj)) {
				saveUpdate.add(yjfj);
			}
		}else{
			saveUpdate.remove(yjfj);
		}
    	return yjfj;
    }
    
    private static boolean delete(Yjfj yjfj,boolean isFlush){
    	clearCache(yjfj,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yjfj)) {
				saveUpdate.remove(yjfj);
				saveInsert.remove(yjfj);
				saveDelete.add(yjfj);
			}
    	}else{
    		saveUpdate.remove(yjfj);
			saveInsert.remove(yjfj);
			saveDelete.remove(yjfj);
    	}
    	return false;
    }
    
    public static Yjfj updateAndFlush(Yjfj yjfj){
    	update(yjfj,true);
    	return YjfjJedis.update(yjfj);
    }
    
    public static Yjfj insertAndFlush(Yjfj yjfj){
    	int id = LASTID.get();
    	insert(yjfj,true);
    	if(id > 0){
    		yjfj = YjfjJedis.insert(yjfj);
    	}
    	return yjfj;
    }
    
    public static boolean deleteAndFlush(Yjfj yjfj){
    	delete(yjfj,true);
    	return YjfjJedis.delete(yjfj);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YjfjDao.deleteBatch(saveDelete);
    	YjfjDao.insertBatch(saveInsert);
    	YjfjDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yjfj yjfj){
    	if (saveDelete.remove(yjfj))
			YjfjDao.delete(yjfj);
		if (saveInsert.remove(yjfj))
			YjfjDao.insert(yjfj);
		if (saveUpdate.remove(yjfj))
			YjfjDao.update(yjfj);
    }
    
    public static void saveDatabase(List<Yjfj> yjfjs){
    	if (saveDelete.removeAll(yjfjs))
			YjfjDao.deleteBatch(yjfjs);
		if (saveInsert.removeAll(yjfjs))
			YjfjDao.insertBatch(yjfjs);
		if (saveUpdate.removeAll(yjfjs))
			YjfjDao.updateBatch(yjfjs);
    }
    
    public static void saveRedis(){
    	YjfjJedis.clearCaches(saveDelete);
    	YjfjJedis.loadCaches(saveInsert);
    	YjfjJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yjfj yjfj){
    	if (saveDelete.remove(yjfj))
			YjfjJedis.clearCache(yjfj);
		if (saveInsert.remove(yjfj))
			YjfjJedis.loadCache(yjfj);
		if (saveUpdate.remove(yjfj))
			YjfjJedis.loadCache(yjfj);
    }
    
    public static void saveRedis(List<Yjfj> yjfjs){
    	if (saveDelete.removeAll(yjfjs))
			YjfjJedis.clearCaches(yjfjs);
		if (saveInsert.removeAll(yjfjs))
			YjfjJedis.loadCaches(yjfjs);
		if (saveUpdate.removeAll(yjfjs))
			YjfjJedis.loadCaches(yjfjs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yjfj yjfj){
   		saveDatabase(yjfj);
   		saveRedis(yjfj);
    }
    
    public static void saveAll(List<Yjfj> yjfjs){
   		saveDatabase(yjfjs);
   		saveRedis(yjfjs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yjfj yjfj){
	
		loadCache(yjfj);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yjfj> yjfjs){
		for(Yjfj yjfj : yjfjs){
			loadCacheCascade(yjfj);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yjfj yjfj,int saveType){
	
		clearCache(yjfj,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yjfj> yjfjs,int saveType){
		for(Yjfj yjfj : yjfjs){
			clearCacheCascade(yjfj,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yjfjs);
			break;
		case 2:
			saveRedis(yjfjs);
			break;
		case 3:
			saveAll(yjfjs);
			break;
		default:
			break;
		}
	}
}
package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.db.jedis.YhJedis;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.dao.YhDao;
import com.sandking.db.bean.Yh;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
/**
 * 用户
 */
public class YhCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yh> idCache = SK_Collections.newSortedMap();
	// static final SK_IndexMap<String, String> ncCache = new SK_IndexMap<String, String>();
	static final Map<String, String> ncCache = new ConcurrentHashMap<String, String>();
	// static final SK_IndexMap<String, String> zhCache = new SK_IndexMap<String, String>();
	static final Map<String, String> zhCache = new ConcurrentHashMap<String, String>();
	// static final SK_IndexMap<String, String> zhmmCache = new SK_IndexMap<String, String>();
	static final Map<String, String> zhmmCache = new ConcurrentHashMap<String, String>();
	static final Map<String, Set<String>> yhlx_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> fwq_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> qdCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> lm_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yh> saveDelete = new CopyOnWriteArrayList<Yh>();
	
	static final List<Yh> saveInsert = new CopyOnWriteArrayList<Yh>();
	
	static final List<Yh> saveUpdate = new CopyOnWriteArrayList<Yh>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yh getById(int id){
		Yh yh = null;
		String key = SK_Plus.b(id).e();
		yh = idCache.get(key);
		
		if(yh==null){
			//查询数据库
			yh = YhJedis.getById(id);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static Yh getByNc(String nc){
		Yh yh = null;
		String key = SK_Plus.b(nc).e();
		key = ncCache.get(key);
		if(key!=null){
			yh = idCache.get(key);	
		} 
		if(yh==null){
			//查询数据库
			yh = YhJedis.getByNc(nc);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static Yh getByZh(String zh){
		Yh yh = null;
		String key = SK_Plus.b(zh).e();
		key = zhCache.get(key);
		if(key!=null){
			yh = idCache.get(key);	
		} 
		if(yh==null){
			//查询数据库
			yh = YhJedis.getByZh(zh);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static Yh getByZhMm(String zh, String mm){
		Yh yh = null;
		String key = SK_Plus.b(zh,mm).e();
		key = zhmmCache.get(key);
		if(key!=null){
			yh = idCache.get(key);	
		} 
		if(yh==null){
			//查询数据库
			yh = YhJedis.getByZhMm(zh, mm);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<Yh> getByYhlx_id(int yhlx_id){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(yhlx_id).e();
		Set<String> keys = yhlx_idCache.get(key);
		if(keys != null){
			Yh yh = null;
			for (String k : keys) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhJedis.getByYhlx_id(yhlx_id);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,int page,int size,Integer pageCount){
		List<Yh> yhs = getByYhlx_id(yhlx_id);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<Yh> getByFwq_id(int fwq_id){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(fwq_id).e();
		Set<String> keys = fwq_idCache.get(key);
		if(keys != null){
			Yh yh = null;
			for (String k : keys) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhJedis.getByFwq_id(fwq_id);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageFwq_id(int fwq_id,int page,int size,Integer pageCount){
		List<Yh> yhs = getByFwq_id(fwq_id);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<Yh> getByQd(String qd){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(qd).e();
		Set<String> keys = qdCache.get(key);
		if(keys != null){
			Yh yh = null;
			for (String k : keys) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhJedis.getByQd(qd);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageQd(String qd,int page,int size,Integer pageCount){
		List<Yh> yhs = getByQd(qd);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Yh> getByLm_id(int lm_id){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys != null){
			Yh yh = null;
			for (String k : keys) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhJedis.getByLm_id(lm_id);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageLm_id(int lm_id,int page,int size,Integer pageCount){
		List<Yh> yhs = getByLm_id(lm_id);
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	
	public static List<Yh> getCacheAll(){
		return new ArrayList<Yh>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yh> getAll(){
		List<Yh> yhs = new ArrayList<Yh>();
		if(!isLoadAll){
			yhs = YhJedis.getAll();
			loadCaches(yhs);
			isLoadAll = true;
		}else{
			yhs = new ArrayList<Yh>(idCache.values());
		}
		return yhs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yh> getAllByPage(int page,int size,Integer pageCount){
		List<Yh> yhs = getAll();
		yhs = SK_List.getPage(yhs, page, size, pageCount);
		return yhs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yh> yhs){
		for(Yh yh : yhs){
			loadCache(yh);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yh yh){
		idCache.put(SK_Plus.b(yh.getId()).e(),yh);
		ncCache.put(SK_Plus.b(yh.getNc()).e(),String.valueOf(yh.getId()));
		zhCache.put(SK_Plus.b(yh.getZh()).e(),String.valueOf(yh.getId()));
		zhmmCache.put(SK_Plus.b(yh.getZh(),yh.getMm()).e(),String.valueOf(yh.getId()));
		Set<String> yhlx_idset = yhlx_idCache.get(String.valueOf(SK_Plus.b(yh.getYhlx_id()).e()));
		if(yhlx_idset == null){
			yhlx_idset = new HashSet<String>();
		}
		yhlx_idset.add(String.valueOf(yh.getId()));
		yhlx_idCache.put(SK_Plus.b(yh.getYhlx_id()).e(),yhlx_idset);
		Set<String> fwq_idset = fwq_idCache.get(String.valueOf(SK_Plus.b(yh.getFwq_id()).e()));
		if(fwq_idset == null){
			fwq_idset = new HashSet<String>();
		}
		fwq_idset.add(String.valueOf(yh.getId()));
		fwq_idCache.put(SK_Plus.b(yh.getFwq_id()).e(),fwq_idset);
		Set<String> qdset = qdCache.get(String.valueOf(SK_Plus.b(yh.getQd()).e()));
		if(qdset == null){
			qdset = new HashSet<String>();
		}
		qdset.add(String.valueOf(yh.getId()));
		qdCache.put(SK_Plus.b(yh.getQd()).e(),qdset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(yh.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		lm_idset.add(String.valueOf(yh.getId()));
		lm_idCache.put(SK_Plus.b(yh.getLm_id()).e(),lm_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yh yh,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yh.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yh.getId()).e());
		}
		ncCache.remove(yh.getNcIndex());
		zhCache.remove(yh.getZhIndex());
		zhmmCache.remove(yh.getZhMmIndex());
		Set<String> yhlx_idset = yhlx_idCache.get(String.valueOf(SK_Plus.b(yh.getYhlx_id()).e()));
		if(yhlx_idset == null){
			yhlx_idset = new HashSet<String>();
		}
		if (yhlx_idset.contains(String.valueOf(yh.getId()))) {
			yhlx_idset.remove(String.valueOf(yh.getId()));
		}
		yhlx_idCache.put(SK_Plus.b(yh.getYhlx_id()).e(),yhlx_idset);
		Set<String> fwq_idset = fwq_idCache.get(String.valueOf(SK_Plus.b(yh.getFwq_id()).e()));
		if(fwq_idset == null){
			fwq_idset = new HashSet<String>();
		}
		if (fwq_idset.contains(String.valueOf(yh.getId()))) {
			fwq_idset.remove(String.valueOf(yh.getId()));
		}
		fwq_idCache.put(SK_Plus.b(yh.getFwq_id()).e(),fwq_idset);
		Set<String> qdset = qdCache.get(String.valueOf(SK_Plus.b(yh.getQd()).e()));
		if(qdset == null){
			qdset = new HashSet<String>();
		}
		if (qdset.contains(String.valueOf(yh.getId()))) {
			qdset.remove(String.valueOf(yh.getId()));
		}
		qdCache.put(SK_Plus.b(yh.getQd()).e(),qdset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(yh.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		if (lm_idset.contains(String.valueOf(yh.getId()))) {
			lm_idset.remove(String.valueOf(yh.getId()));
		}
		lm_idCache.put(SK_Plus.b(yh.getLm_id()).e(),lm_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yh);
			break;
		case 2:
			saveRedis(yh);
			break;
		case 3:
			saveAll(yh);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yh> yhs,int clearType,int saveType){
		for(Yh yh : yhs){
			clearCache(yh,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhs);
			break;
		case 2:
			saveRedis(yhs);
			break;
		case 3:
			saveAll(yhs);
			break;
		default:
			break;
		}
	}
	
	public static Yh insert(Yh yh){
		return insert(yh,false);
    }
    
    public static Yh update(Yh yh){
    	return update(yh,false);
    }
    
    public static boolean delete(Yh yh){
    	return delete(yh,false);
    }
    
    private static Yh insert(Yh yh,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yh = YhJedis.insert(yh);
    		LASTID.set(yh.getId());
    	}else{
    		int _id = yh.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yh.getId() > id) {
				LASTID.set(yh.getId());
			}
    		//加入定时器
    	}
    	loadCache(yh);
    	if(!isFlush){
	    	if (!saveInsert.contains(yh)) {
				saveInsert.add(yh);
			}
		}
    	return yh;
    }
    
    private static Yh update(Yh yh,boolean isFlush){
    	clearCache(yh,1,0);
    	loadCache(yh);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yh)) {
				saveUpdate.add(yh);
			}
		}else{
			saveUpdate.remove(yh);
		}
    	return yh;
    }
    
    private static boolean delete(Yh yh,boolean isFlush){
    	clearCache(yh,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yh)) {
				saveUpdate.remove(yh);
				saveInsert.remove(yh);
				saveDelete.add(yh);
			}
    	}else{
    		saveUpdate.remove(yh);
			saveInsert.remove(yh);
			saveDelete.remove(yh);
    	}
    	return false;
    }
    
    public static Yh updateAndFlush(Yh yh){
    	update(yh,true);
    	return YhJedis.update(yh);
    }
    
    public static Yh insertAndFlush(Yh yh){
    	int id = LASTID.get();
    	insert(yh,true);
    	if(id > 0){
    		yh = YhJedis.insert(yh);
    	}
    	return yh;
    }
    
    public static boolean deleteAndFlush(Yh yh){
    	delete(yh,true);
    	return YhJedis.delete(yh);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhDao.deleteBatch(saveDelete);
    	YhDao.insertBatch(saveInsert);
    	YhDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yh yh){
    	if (saveDelete.remove(yh))
			YhDao.delete(yh);
		if (saveInsert.remove(yh))
			YhDao.insert(yh);
		if (saveUpdate.remove(yh))
			YhDao.update(yh);
    }
    
    public static void saveDatabase(List<Yh> yhs){
    	if (saveDelete.removeAll(yhs))
			YhDao.deleteBatch(yhs);
		if (saveInsert.removeAll(yhs))
			YhDao.insertBatch(yhs);
		if (saveUpdate.removeAll(yhs))
			YhDao.updateBatch(yhs);
    }
    
    public static void saveRedis(){
    	YhJedis.clearCaches(saveDelete);
    	YhJedis.loadCaches(saveInsert);
    	YhJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yh yh){
    	if (saveDelete.remove(yh))
			YhJedis.clearCache(yh);
		if (saveInsert.remove(yh))
			YhJedis.loadCache(yh);
		if (saveUpdate.remove(yh))
			YhJedis.loadCache(yh);
    }
    
    public static void saveRedis(List<Yh> yhs){
    	if (saveDelete.removeAll(yhs))
			YhJedis.clearCaches(yhs);
		if (saveInsert.removeAll(yhs))
			YhJedis.loadCaches(yhs);
		if (saveUpdate.removeAll(yhs))
			YhJedis.loadCaches(yhs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yh yh){
   		saveDatabase(yh);
   		saveRedis(yh);
    }
    
    public static void saveAll(List<Yh> yhs){
   		saveDatabase(yhs);
   		saveRedis(yhs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yh yh){
	
		loadCache(yh);
		
		YhjzCache.loadCachesCascade(YhjzCache.getByYh_id(yh.getId()));
		YhysCache.loadCachesCascade(YhysCache.getByYh_id(yh.getId()));
		YhbbCache.loadCachesCascade(YhbbCache.getByYh_id(yh.getId()));
		YhyxCache.loadCachesCascade(YhyxCache.getByYh_id(yh.getId()));
		YhzsCache.loadCachesCascade(YhzsCache.getByYh_id(yh.getId()));
		YhyjCache.loadCachesCascade(YhyjCache.getByFjrid(yh.getId()));
		YhyjCache.loadCachesCascade(YhyjCache.getByJsrid(yh.getId()));
		YhxjCache.loadCachesCascade(YhxjCache.getByYh_id(yh.getId()));
		YhzaCache.loadCachesCascade(YhzaCache.getByYh_id(yh.getId()));
		LtCache.loadCachesCascade(LtCache.getByJsrid(yh.getId()));
		LtCache.loadCachesCascade(LtCache.getByFyrid(yh.getId()));
		LmCache.loadCachesCascade(LmCache.getByCjrid(yh.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yh> yhs){
		for(Yh yh : yhs){
			loadCacheCascade(yh);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yh yh,int saveType){
	
		clearCache(yh,3,saveType);
		
		YhjzCache.clearCachesCascade(YhjzCache.getByYh_id(yh.getId()),saveType);
		YhysCache.clearCachesCascade(YhysCache.getByYh_id(yh.getId()),saveType);
		YhbbCache.clearCachesCascade(YhbbCache.getByYh_id(yh.getId()),saveType);
		YhyxCache.clearCachesCascade(YhyxCache.getByYh_id(yh.getId()),saveType);
		YhzsCache.clearCachesCascade(YhzsCache.getByYh_id(yh.getId()),saveType);
		YhyjCache.clearCachesCascade(YhyjCache.getByFjrid(yh.getId()),saveType);
		YhyjCache.clearCachesCascade(YhyjCache.getByJsrid(yh.getId()),saveType);
		YhxjCache.clearCachesCascade(YhxjCache.getByYh_id(yh.getId()),saveType);
		YhzaCache.clearCachesCascade(YhzaCache.getByYh_id(yh.getId()),saveType);
		LtCache.clearCachesCascade(LtCache.getByJsrid(yh.getId()),saveType);
		LtCache.clearCachesCascade(LtCache.getByFyrid(yh.getId()),saveType);
		LmCache.clearCachesCascade(LmCache.getByCjrid(yh.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yh> yhs,int saveType){
		for(Yh yh : yhs){
			clearCacheCascade(yh,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhs);
			break;
		case 2:
			saveRedis(yhs);
			break;
		case 3:
			saveAll(yhs);
			break;
		default:
			break;
		}
	}
}
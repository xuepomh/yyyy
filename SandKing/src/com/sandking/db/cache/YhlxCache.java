package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.dao.YhlxDao;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.bean.Yhlx;
import com.sandking.tools.SK_List;
import com.sandking.db.jedis.YhlxJedis;
/**
 * 用户类型
 */
public class YhlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhlxCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhlx> idCache = SK_Collections.newSortedMap();
	
	static final List<Yhlx> saveDelete = new CopyOnWriteArrayList<Yhlx>();
	
	static final List<Yhlx> saveInsert = new CopyOnWriteArrayList<Yhlx>();
	
	static final List<Yhlx> saveUpdate = new CopyOnWriteArrayList<Yhlx>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhlx getById(int id){
		Yhlx yhlx = null;
		String key = SK_Plus.b(id).e();
		yhlx = idCache.get(key);
		
		if(yhlx==null){
			//查询数据库
			yhlx = YhlxJedis.getById(id);
			if(yhlx!=null){
				//加入缓存
				loadCache(yhlx);
			}
		}
		return yhlx;
	}
	
	
	public static List<Yhlx> getCacheAll(){
		return new ArrayList<Yhlx>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhlx> getAll(){
		List<Yhlx> yhlxs = new ArrayList<Yhlx>();
		if(!isLoadAll){
			yhlxs = YhlxJedis.getAll();
			loadCaches(yhlxs);
			isLoadAll = true;
		}else{
			yhlxs = new ArrayList<Yhlx>(idCache.values());
		}
		return yhlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhlx> getAllByPage(int page,int size,Integer pageCount){
		List<Yhlx> yhlxs = getAll();
		yhlxs = SK_List.getPage(yhlxs, page, size, pageCount);
		return yhlxs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhlx> yhlxs){
		for(Yhlx yhlx : yhlxs){
			loadCache(yhlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhlx yhlx){
		idCache.put(SK_Plus.b(yhlx.getId()).e(),yhlx);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhlx yhlx,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhlx.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhlx.getId()).e());
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhlx);
			break;
		case 2:
			saveRedis(yhlx);
			break;
		case 3:
			saveAll(yhlx);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhlx> yhlxs,int clearType,int saveType){
		for(Yhlx yhlx : yhlxs){
			clearCache(yhlx,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhlxs);
			break;
		case 2:
			saveRedis(yhlxs);
			break;
		case 3:
			saveAll(yhlxs);
			break;
		default:
			break;
		}
	}
	
	public static Yhlx insert(Yhlx yhlx){
		return insert(yhlx,false);
    }
    
    public static Yhlx update(Yhlx yhlx){
    	return update(yhlx,false);
    }
    
    public static boolean delete(Yhlx yhlx){
    	return delete(yhlx,false);
    }
    
    private static Yhlx insert(Yhlx yhlx,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhlx = YhlxJedis.insert(yhlx);
    		LASTID.set(yhlx.getId());
    	}else{
    		int _id = yhlx.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhlx.getId() > id) {
				LASTID.set(yhlx.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhlx);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhlx)) {
				saveInsert.add(yhlx);
			}
		}
    	return yhlx;
    }
    
    private static Yhlx update(Yhlx yhlx,boolean isFlush){
    	clearCache(yhlx,1,0);
    	loadCache(yhlx);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhlx)) {
				saveUpdate.add(yhlx);
			}
		}else{
			saveUpdate.remove(yhlx);
		}
    	return yhlx;
    }
    
    private static boolean delete(Yhlx yhlx,boolean isFlush){
    	clearCache(yhlx,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhlx)) {
				saveUpdate.remove(yhlx);
				saveInsert.remove(yhlx);
				saveDelete.add(yhlx);
			}
    	}else{
    		saveUpdate.remove(yhlx);
			saveInsert.remove(yhlx);
			saveDelete.remove(yhlx);
    	}
    	return false;
    }
    
    public static Yhlx updateAndFlush(Yhlx yhlx){
    	update(yhlx,true);
    	return YhlxJedis.update(yhlx);
    }
    
    public static Yhlx insertAndFlush(Yhlx yhlx){
    	int id = LASTID.get();
    	insert(yhlx,true);
    	if(id > 0){
    		yhlx = YhlxJedis.insert(yhlx);
    	}
    	return yhlx;
    }
    
    public static boolean deleteAndFlush(Yhlx yhlx){
    	delete(yhlx,true);
    	return YhlxJedis.delete(yhlx);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhlxDao.deleteBatch(saveDelete);
    	YhlxDao.insertBatch(saveInsert);
    	YhlxDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhlx yhlx){
    	if (saveDelete.remove(yhlx))
			YhlxDao.delete(yhlx);
		if (saveInsert.remove(yhlx))
			YhlxDao.insert(yhlx);
		if (saveUpdate.remove(yhlx))
			YhlxDao.update(yhlx);
    }
    
    public static void saveDatabase(List<Yhlx> yhlxs){
    	if (saveDelete.removeAll(yhlxs))
			YhlxDao.deleteBatch(yhlxs);
		if (saveInsert.removeAll(yhlxs))
			YhlxDao.insertBatch(yhlxs);
		if (saveUpdate.removeAll(yhlxs))
			YhlxDao.updateBatch(yhlxs);
    }
    
    public static void saveRedis(){
    	YhlxJedis.clearCaches(saveDelete);
    	YhlxJedis.loadCaches(saveInsert);
    	YhlxJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhlx yhlx){
    	if (saveDelete.remove(yhlx))
			YhlxJedis.clearCache(yhlx);
		if (saveInsert.remove(yhlx))
			YhlxJedis.loadCache(yhlx);
		if (saveUpdate.remove(yhlx))
			YhlxJedis.loadCache(yhlx);
    }
    
    public static void saveRedis(List<Yhlx> yhlxs){
    	if (saveDelete.removeAll(yhlxs))
			YhlxJedis.clearCaches(yhlxs);
		if (saveInsert.removeAll(yhlxs))
			YhlxJedis.loadCaches(yhlxs);
		if (saveUpdate.removeAll(yhlxs))
			YhlxJedis.loadCaches(yhlxs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhlx yhlx){
   		saveDatabase(yhlx);
   		saveRedis(yhlx);
    }
    
    public static void saveAll(List<Yhlx> yhlxs){
   		saveDatabase(yhlxs);
   		saveRedis(yhlxs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhlx yhlx){
	
		loadCache(yhlx);
		
		YhCache.loadCachesCascade(YhCache.getByYhlx_id(yhlx.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhlx> yhlxs){
		for(Yhlx yhlx : yhlxs){
			loadCacheCascade(yhlx);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhlx yhlx,int saveType){
	
		clearCache(yhlx,3,saveType);
		
		YhCache.clearCachesCascade(YhCache.getByYhlx_id(yhlx.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhlx> yhlxs,int saveType){
		for(Yhlx yhlx : yhlxs){
			clearCacheCascade(yhlx,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhlxs);
			break;
		case 2:
			saveRedis(yhlxs);
			break;
		case 3:
			saveAll(yhlxs);
			break;
		default:
			break;
		}
	}
}
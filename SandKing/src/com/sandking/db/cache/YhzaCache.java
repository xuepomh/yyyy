package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.jedis.YhzaJedis;
import java.util.Set;
import com.sandking.db.bean.Yhza;
import com.sandking.db.dao.YhzaDao;
/**
 * 用户障碍
 */
public class YhzaCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhzaCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhza> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhza> saveDelete = new CopyOnWriteArrayList<Yhza>();
	
	static final List<Yhza> saveInsert = new CopyOnWriteArrayList<Yhza>();
	
	static final List<Yhza> saveUpdate = new CopyOnWriteArrayList<Yhza>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhza getById(int id){
		Yhza yhza = null;
		String key = SK_Plus.b(id).e();
		yhza = idCache.get(key);
		
		if(yhza==null){
			//查询数据库
			yhza = YhzaJedis.getById(id);
			if(yhza!=null){
				//加入缓存
				loadCache(yhza);
			}
		}
		return yhza;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhza> getByYh_id(int yh_id){
		List<Yhza> yhzas = new ArrayList<Yhza>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhza yhza = null;
			for (String k : keys) {
				yhza = getById(Integer.valueOf(k));
				if (yhza == null) continue;
					yhzas.add(yhza);
			}
		}else{
			yhzas = YhzaJedis.getByYh_id(yh_id);
			if(!yhzas.isEmpty()){
				loadCaches(yhzas);
			}
		}
		return yhzas;
	}
	
	public static List<Yhza> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhza> yhzas = getByYh_id(yh_id);
		yhzas = SK_List.getPage(yhzas, page, size, pageCount);
		return yhzas;
	}
	
	public static List<Yhza> getCacheAll(){
		return new ArrayList<Yhza>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhza> getAll(){
		List<Yhza> yhzas = new ArrayList<Yhza>();
		if(!isLoadAll){
			yhzas = YhzaJedis.getAll();
			loadCaches(yhzas);
			isLoadAll = true;
		}else{
			yhzas = new ArrayList<Yhza>(idCache.values());
		}
		return yhzas;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhza> getAllByPage(int page,int size,Integer pageCount){
		List<Yhza> yhzas = getAll();
		yhzas = SK_List.getPage(yhzas, page, size, pageCount);
		return yhzas;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhza> yhzas){
		for(Yhza yhza : yhzas){
			loadCache(yhza);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhza yhza){
		idCache.put(SK_Plus.b(yhza.getId()).e(),yhza);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhza.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhza.getId()));
		yh_idCache.put(SK_Plus.b(yhza.getYh_id()).e(),yh_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhza yhza,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhza.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhza.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhza.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhza.getId()))) {
			yh_idset.remove(String.valueOf(yhza.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhza.getYh_id()).e(),yh_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yhza);
			break;
		case 2:
			saveRedis(yhza);
			break;
		case 3:
			saveAll(yhza);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhza> yhzas,int clearType,int saveType){
		for(Yhza yhza : yhzas){
			clearCache(yhza,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhzas);
			break;
		case 2:
			saveRedis(yhzas);
			break;
		case 3:
			saveAll(yhzas);
			break;
		default:
			break;
		}
	}
	
	public static Yhza insert(Yhza yhza){
		return insert(yhza,false);
    }
    
    public static Yhza update(Yhza yhza){
    	return update(yhza,false);
    }
    
    public static boolean delete(Yhza yhza){
    	return delete(yhza,false);
    }
    
    private static Yhza insert(Yhza yhza,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhza = YhzaJedis.insert(yhza);
    		LASTID.set(yhza.getId());
    	}else{
    		int _id = yhza.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhza.getId() > id) {
				LASTID.set(yhza.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhza);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhza)) {
				saveInsert.add(yhza);
			}
		}
    	return yhza;
    }
    
    private static Yhza update(Yhza yhza,boolean isFlush){
    	clearCache(yhza,1,0);
    	loadCache(yhza);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhza)) {
				saveUpdate.add(yhza);
			}
		}else{
			saveUpdate.remove(yhza);
		}
    	return yhza;
    }
    
    private static boolean delete(Yhza yhza,boolean isFlush){
    	clearCache(yhza,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhza)) {
				saveUpdate.remove(yhza);
				saveInsert.remove(yhza);
				saveDelete.add(yhza);
			}
    	}else{
    		saveUpdate.remove(yhza);
			saveInsert.remove(yhza);
			saveDelete.remove(yhza);
    	}
    	return false;
    }
    
    public static Yhza updateAndFlush(Yhza yhza){
    	update(yhza,true);
    	return YhzaJedis.update(yhza);
    }
    
    public static Yhza insertAndFlush(Yhza yhza){
    	int id = LASTID.get();
    	insert(yhza,true);
    	if(id > 0){
    		yhza = YhzaJedis.insert(yhza);
    	}
    	return yhza;
    }
    
    public static boolean deleteAndFlush(Yhza yhza){
    	delete(yhza,true);
    	return YhzaJedis.delete(yhza);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhzaDao.deleteBatch(saveDelete);
    	YhzaDao.insertBatch(saveInsert);
    	YhzaDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhza yhza){
    	if (saveDelete.remove(yhza))
			YhzaDao.delete(yhza);
		if (saveInsert.remove(yhza))
			YhzaDao.insert(yhza);
		if (saveUpdate.remove(yhza))
			YhzaDao.update(yhza);
    }
    
    public static void saveDatabase(List<Yhza> yhzas){
    	if (saveDelete.removeAll(yhzas))
			YhzaDao.deleteBatch(yhzas);
		if (saveInsert.removeAll(yhzas))
			YhzaDao.insertBatch(yhzas);
		if (saveUpdate.removeAll(yhzas))
			YhzaDao.updateBatch(yhzas);
    }
    
    public static void saveRedis(){
    	YhzaJedis.clearCaches(saveDelete);
    	YhzaJedis.loadCaches(saveInsert);
    	YhzaJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhza yhza){
    	if (saveDelete.remove(yhza))
			YhzaJedis.clearCache(yhza);
		if (saveInsert.remove(yhza))
			YhzaJedis.loadCache(yhza);
		if (saveUpdate.remove(yhza))
			YhzaJedis.loadCache(yhza);
    }
    
    public static void saveRedis(List<Yhza> yhzas){
    	if (saveDelete.removeAll(yhzas))
			YhzaJedis.clearCaches(yhzas);
		if (saveInsert.removeAll(yhzas))
			YhzaJedis.loadCaches(yhzas);
		if (saveUpdate.removeAll(yhzas))
			YhzaJedis.loadCaches(yhzas);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhza yhza){
   		saveDatabase(yhza);
   		saveRedis(yhza);
    }
    
    public static void saveAll(List<Yhza> yhzas){
   		saveDatabase(yhzas);
   		saveRedis(yhzas);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhza yhza){
	
		loadCache(yhza);
		
		ZadlCache.loadCachesCascade(ZadlCache.getByYhza_id(yhza.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhza> yhzas){
		for(Yhza yhza : yhzas){
			loadCacheCascade(yhza);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhza yhza,int saveType){
	
		clearCache(yhza,3,saveType);
		
		ZadlCache.clearCachesCascade(ZadlCache.getByYhza_id(yhza.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhza> yhzas,int saveType){
		for(Yhza yhza : yhzas){
			clearCacheCascade(yhza,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhzas);
			break;
		case 2:
			saveRedis(yhzas);
			break;
		case 3:
			saveAll(yhzas);
			break;
		default:
			break;
		}
	}
}
package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.jedis.FwqJedis;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.db.dao.FwqDao;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.bean.Fwq;
import com.sandking.tools.SK_List;
/**
 * 服务器
 */
public class FwqCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		FwqCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Fwq> idCache = SK_Collections.newSortedMap();
	
	static final List<Fwq> saveDelete = new CopyOnWriteArrayList<Fwq>();
	
	static final List<Fwq> saveInsert = new CopyOnWriteArrayList<Fwq>();
	
	static final List<Fwq> saveUpdate = new CopyOnWriteArrayList<Fwq>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Fwq getById(int id){
		Fwq fwq = null;
		String key = SK_Plus.b(id).e();
		fwq = idCache.get(key);
		
		if(fwq==null){
			//查询数据库
			fwq = FwqJedis.getById(id);
			if(fwq!=null){
				//加入缓存
				loadCache(fwq);
			}
		}
		return fwq;
	}
	
	
	public static List<Fwq> getCacheAll(){
		return new ArrayList<Fwq>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fwq> getAll(){
		List<Fwq> fwqs = new ArrayList<Fwq>();
		if(!isLoadAll){
			fwqs = FwqJedis.getAll();
			loadCaches(fwqs);
			isLoadAll = true;
		}else{
			fwqs = new ArrayList<Fwq>(idCache.values());
		}
		return fwqs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fwq> getAllByPage(int page,int size,Integer pageCount){
		List<Fwq> fwqs = getAll();
		fwqs = SK_List.getPage(fwqs, page, size, pageCount);
		return fwqs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Fwq> fwqs){
		for(Fwq fwq : fwqs){
			loadCache(fwq);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Fwq fwq){
		idCache.put(SK_Plus.b(fwq.getId()).e(),fwq);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Fwq fwq,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(fwq.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(fwq.getId()).e());
		}
		switch (saveType) {
		case 1:
			saveDatabase(fwq);
			break;
		case 2:
			saveRedis(fwq);
			break;
		case 3:
			saveAll(fwq);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Fwq> fwqs,int clearType,int saveType){
		for(Fwq fwq : fwqs){
			clearCache(fwq,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(fwqs);
			break;
		case 2:
			saveRedis(fwqs);
			break;
		case 3:
			saveAll(fwqs);
			break;
		default:
			break;
		}
	}
	
	public static Fwq insert(Fwq fwq){
		return insert(fwq,false);
    }
    
    public static Fwq update(Fwq fwq){
    	return update(fwq,false);
    }
    
    public static boolean delete(Fwq fwq){
    	return delete(fwq,false);
    }
    
    private static Fwq insert(Fwq fwq,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		fwq = FwqJedis.insert(fwq);
    		LASTID.set(fwq.getId());
    	}else{
    		int _id = fwq.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (fwq.getId() > id) {
				LASTID.set(fwq.getId());
			}
    		//加入定时器
    	}
    	loadCache(fwq);
    	if(!isFlush){
	    	if (!saveInsert.contains(fwq)) {
				saveInsert.add(fwq);
			}
		}
    	return fwq;
    }
    
    private static Fwq update(Fwq fwq,boolean isFlush){
    	clearCache(fwq,1,0);
    	loadCache(fwq);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(fwq)) {
				saveUpdate.add(fwq);
			}
		}else{
			saveUpdate.remove(fwq);
		}
    	return fwq;
    }
    
    private static boolean delete(Fwq fwq,boolean isFlush){
    	clearCache(fwq,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(fwq)) {
				saveUpdate.remove(fwq);
				saveInsert.remove(fwq);
				saveDelete.add(fwq);
			}
    	}else{
    		saveUpdate.remove(fwq);
			saveInsert.remove(fwq);
			saveDelete.remove(fwq);
    	}
    	return false;
    }
    
    public static Fwq updateAndFlush(Fwq fwq){
    	update(fwq,true);
    	return FwqJedis.update(fwq);
    }
    
    public static Fwq insertAndFlush(Fwq fwq){
    	int id = LASTID.get();
    	insert(fwq,true);
    	if(id > 0){
    		fwq = FwqJedis.insert(fwq);
    	}
    	return fwq;
    }
    
    public static boolean deleteAndFlush(Fwq fwq){
    	delete(fwq,true);
    	return FwqJedis.delete(fwq);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	FwqDao.deleteBatch(saveDelete);
    	FwqDao.insertBatch(saveInsert);
    	FwqDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Fwq fwq){
    	if (saveDelete.remove(fwq))
			FwqDao.delete(fwq);
		if (saveInsert.remove(fwq))
			FwqDao.insert(fwq);
		if (saveUpdate.remove(fwq))
			FwqDao.update(fwq);
    }
    
    public static void saveDatabase(List<Fwq> fwqs){
    	if (saveDelete.removeAll(fwqs))
			FwqDao.deleteBatch(fwqs);
		if (saveInsert.removeAll(fwqs))
			FwqDao.insertBatch(fwqs);
		if (saveUpdate.removeAll(fwqs))
			FwqDao.updateBatch(fwqs);
    }
    
    public static void saveRedis(){
    	FwqJedis.clearCaches(saveDelete);
    	FwqJedis.loadCaches(saveInsert);
    	FwqJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Fwq fwq){
    	if (saveDelete.remove(fwq))
			FwqJedis.clearCache(fwq);
		if (saveInsert.remove(fwq))
			FwqJedis.loadCache(fwq);
		if (saveUpdate.remove(fwq))
			FwqJedis.loadCache(fwq);
    }
    
    public static void saveRedis(List<Fwq> fwqs){
    	if (saveDelete.removeAll(fwqs))
			FwqJedis.clearCaches(fwqs);
		if (saveInsert.removeAll(fwqs))
			FwqJedis.loadCaches(fwqs);
		if (saveUpdate.removeAll(fwqs))
			FwqJedis.loadCaches(fwqs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Fwq fwq){
   		saveDatabase(fwq);
   		saveRedis(fwq);
    }
    
    public static void saveAll(List<Fwq> fwqs){
   		saveDatabase(fwqs);
   		saveRedis(fwqs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Fwq fwq){
	
		loadCache(fwq);
		
		YhCache.loadCachesCascade(YhCache.getByFwq_id(fwq.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Fwq> fwqs){
		for(Fwq fwq : fwqs){
			loadCacheCascade(fwq);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Fwq fwq,int saveType){
	
		clearCache(fwq,3,saveType);
		
		YhCache.clearCachesCascade(YhCache.getByFwq_id(fwq.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Fwq> fwqs,int saveType){
		for(Fwq fwq : fwqs){
			clearCacheCascade(fwq,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(fwqs);
			break;
		case 2:
			saveRedis(fwqs);
			break;
		case 3:
			saveAll(fwqs);
			break;
		default:
			break;
		}
	}
}
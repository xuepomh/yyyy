package com.sandking.db.cache;

import com.sandking.db.dao.JzdlDao;
import java.util.ArrayList;
import com.sandking.db.bean.Jzdl;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.jedis.JzdlJedis;
import java.util.Set;
/**
 * 建筑队列
 */
public class JzdlCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		JzdlCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Jzdl> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhjz_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Jzdl> saveDelete = new CopyOnWriteArrayList<Jzdl>();
	
	static final List<Jzdl> saveInsert = new CopyOnWriteArrayList<Jzdl>();
	
	static final List<Jzdl> saveUpdate = new CopyOnWriteArrayList<Jzdl>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Jzdl getById(int id){
		Jzdl jzdl = null;
		String key = SK_Plus.b(id).e();
		jzdl = idCache.get(key);
		
		if(jzdl==null){
			//查询数据库
			jzdl = JzdlJedis.getById(id);
			if(jzdl!=null){
				//加入缓存
				loadCache(jzdl);
			}
		}
		return jzdl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Jzdl> getByYhjz_id(int yhjz_id){
		List<Jzdl> jzdls = new ArrayList<Jzdl>();
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys != null){
			Jzdl jzdl = null;
			for (String k : keys) {
				jzdl = getById(Integer.valueOf(k));
				if (jzdl == null) continue;
					jzdls.add(jzdl);
			}
		}else{
			jzdls = JzdlJedis.getByYhjz_id(yhjz_id);
			if(!jzdls.isEmpty()){
				loadCaches(jzdls);
			}
		}
		return jzdls;
	}
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,int page,int size,Integer pageCount){
		List<Jzdl> jzdls = getByYhjz_id(yhjz_id);
		jzdls = SK_List.getPage(jzdls, page, size, pageCount);
		return jzdls;
	}
	
	public static List<Jzdl> getCacheAll(){
		return new ArrayList<Jzdl>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Jzdl> getAll(){
		List<Jzdl> jzdls = new ArrayList<Jzdl>();
		if(!isLoadAll){
			jzdls = JzdlJedis.getAll();
			loadCaches(jzdls);
			isLoadAll = true;
		}else{
			jzdls = new ArrayList<Jzdl>(idCache.values());
		}
		return jzdls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Jzdl> getAllByPage(int page,int size,Integer pageCount){
		List<Jzdl> jzdls = getAll();
		jzdls = SK_List.getPage(jzdls, page, size, pageCount);
		return jzdls;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Jzdl> jzdls){
		for(Jzdl jzdl : jzdls){
			loadCache(jzdl);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Jzdl jzdl){
		idCache.put(SK_Plus.b(jzdl.getId()).e(),jzdl);
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(jzdl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		yhjz_idset.add(String.valueOf(jzdl.getId()));
		yhjz_idCache.put(SK_Plus.b(jzdl.getYhjz_id()).e(),yhjz_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Jzdl jzdl,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(jzdl.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(jzdl.getId()).e());
		}
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(jzdl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		if (yhjz_idset.contains(String.valueOf(jzdl.getId()))) {
			yhjz_idset.remove(String.valueOf(jzdl.getId()));
		}
		yhjz_idCache.put(SK_Plus.b(jzdl.getYhjz_id()).e(),yhjz_idset);
		switch (saveType) {
		case 1:
			saveDatabase(jzdl);
			break;
		case 2:
			saveRedis(jzdl);
			break;
		case 3:
			saveAll(jzdl);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Jzdl> jzdls,int clearType,int saveType){
		for(Jzdl jzdl : jzdls){
			clearCache(jzdl,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(jzdls);
			break;
		case 2:
			saveRedis(jzdls);
			break;
		case 3:
			saveAll(jzdls);
			break;
		default:
			break;
		}
	}
	
	public static Jzdl insert(Jzdl jzdl){
		return insert(jzdl,false);
    }
    
    public static Jzdl update(Jzdl jzdl){
    	return update(jzdl,false);
    }
    
    public static boolean delete(Jzdl jzdl){
    	return delete(jzdl,false);
    }
    
    private static Jzdl insert(Jzdl jzdl,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		jzdl = JzdlJedis.insert(jzdl);
    		LASTID.set(jzdl.getId());
    	}else{
    		int _id = jzdl.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (jzdl.getId() > id) {
				LASTID.set(jzdl.getId());
			}
    		//加入定时器
    	}
    	loadCache(jzdl);
    	if(!isFlush){
	    	if (!saveInsert.contains(jzdl)) {
				saveInsert.add(jzdl);
			}
		}
    	return jzdl;
    }
    
    private static Jzdl update(Jzdl jzdl,boolean isFlush){
    	clearCache(jzdl,1,0);
    	loadCache(jzdl);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(jzdl)) {
				saveUpdate.add(jzdl);
			}
		}else{
			saveUpdate.remove(jzdl);
		}
    	return jzdl;
    }
    
    private static boolean delete(Jzdl jzdl,boolean isFlush){
    	clearCache(jzdl,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(jzdl)) {
				saveUpdate.remove(jzdl);
				saveInsert.remove(jzdl);
				saveDelete.add(jzdl);
			}
    	}else{
    		saveUpdate.remove(jzdl);
			saveInsert.remove(jzdl);
			saveDelete.remove(jzdl);
    	}
    	return false;
    }
    
    public static Jzdl updateAndFlush(Jzdl jzdl){
    	update(jzdl,true);
    	return JzdlJedis.update(jzdl);
    }
    
    public static Jzdl insertAndFlush(Jzdl jzdl){
    	int id = LASTID.get();
    	insert(jzdl,true);
    	if(id > 0){
    		jzdl = JzdlJedis.insert(jzdl);
    	}
    	return jzdl;
    }
    
    public static boolean deleteAndFlush(Jzdl jzdl){
    	delete(jzdl,true);
    	return JzdlJedis.delete(jzdl);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	JzdlDao.deleteBatch(saveDelete);
    	JzdlDao.insertBatch(saveInsert);
    	JzdlDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Jzdl jzdl){
    	if (saveDelete.remove(jzdl))
			JzdlDao.delete(jzdl);
		if (saveInsert.remove(jzdl))
			JzdlDao.insert(jzdl);
		if (saveUpdate.remove(jzdl))
			JzdlDao.update(jzdl);
    }
    
    public static void saveDatabase(List<Jzdl> jzdls){
    	if (saveDelete.removeAll(jzdls))
			JzdlDao.deleteBatch(jzdls);
		if (saveInsert.removeAll(jzdls))
			JzdlDao.insertBatch(jzdls);
		if (saveUpdate.removeAll(jzdls))
			JzdlDao.updateBatch(jzdls);
    }
    
    public static void saveRedis(){
    	JzdlJedis.clearCaches(saveDelete);
    	JzdlJedis.loadCaches(saveInsert);
    	JzdlJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Jzdl jzdl){
    	if (saveDelete.remove(jzdl))
			JzdlJedis.clearCache(jzdl);
		if (saveInsert.remove(jzdl))
			JzdlJedis.loadCache(jzdl);
		if (saveUpdate.remove(jzdl))
			JzdlJedis.loadCache(jzdl);
    }
    
    public static void saveRedis(List<Jzdl> jzdls){
    	if (saveDelete.removeAll(jzdls))
			JzdlJedis.clearCaches(jzdls);
		if (saveInsert.removeAll(jzdls))
			JzdlJedis.loadCaches(jzdls);
		if (saveUpdate.removeAll(jzdls))
			JzdlJedis.loadCaches(jzdls);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Jzdl jzdl){
   		saveDatabase(jzdl);
   		saveRedis(jzdl);
    }
    
    public static void saveAll(List<Jzdl> jzdls){
   		saveDatabase(jzdls);
   		saveRedis(jzdls);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Jzdl jzdl){
	
		loadCache(jzdl);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Jzdl> jzdls){
		for(Jzdl jzdl : jzdls){
			loadCacheCascade(jzdl);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Jzdl jzdl,int saveType){
	
		clearCache(jzdl,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Jzdl> jzdls,int saveType){
		for(Jzdl jzdl : jzdls){
			clearCacheCascade(jzdl,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(jzdls);
			break;
		case 2:
			saveRedis(jzdls);
			break;
		case 3:
			saveAll(jzdls);
			break;
		default:
			break;
		}
	}
}
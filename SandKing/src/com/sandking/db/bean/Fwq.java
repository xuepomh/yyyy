package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.FwqCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 服务器
 */
public class Fwq {

	public static final String TABLENAME = "服务器";
	public static final String CLASSNAME = "Fwq"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	/** 开服时间 */
	private java.util.Date kfsj;
	
	/** 关服时间 */
	private java.util.Date gfsj;
	
	/** serverkey */
	private String serverkey;
	
	/** 顺序 */
	private int sx;
	
	
	public Fwq() {
		super();
	}
	
	public Fwq(int id, String mc, java.util.Date kfsj, java.util.Date gfsj, String serverkey, int sx) {
		super();
		this.id = id;
		this.mc = mc;
		this.kfsj = kfsj;
		this.gfsj = gfsj;
		this.serverkey = serverkey;
		this.sx = sx;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
		addUpdateColumn("名称",mc);
	}
	
	public java.util.Date getKfsj() {
		return kfsj;
	}
	
	public void setKfsj(java.util.Date kfsj) {
		this.kfsj = kfsj;
		addUpdateColumn("开服时间",kfsj);
	}
	
	public java.util.Date getGfsj() {
		return gfsj;
	}
	
	public void setGfsj(java.util.Date gfsj) {
		this.gfsj = gfsj;
		addUpdateColumn("关服时间",gfsj);
	}
	
	public String getServerkey() {
		return serverkey;
	}
	
	public void setServerkey(String serverkey) {
		this.serverkey = serverkey;
		addUpdateColumn("serverkey",serverkey);
	}
	
	public int getSx() {
		return sx;
	}
	
	public void setSx(int sx) {
		this.sx = sx;
		addUpdateColumn("顺序",sx);
	}
	
	public void changeSxWith(int sx){
		this.sx += sx;
		setSx(this.sx);
	}
	
	public void changeSxWithMin(int sx,int min){
		this.sx += sx;
		this.sx = this.sx < min ? min : this.sx;
		setSx(this.sx);
	}
	
	public void changeSxWithMax(int sx,int max){
		this.sx += sx;
		this.sx = this.sx > max ? max : this.sx;
		setSx(this.sx);
	}
	
	public void changeSxWithMaxMin(int sx,int max,int min){
		this.sx += sx;
		this.sx = this.sx < min ? min : this.sx;
		this.sx = this.sx > max ? max : this.sx;
		setSx(this.sx);
	}	
	
	//id
	public List<Yh> getYhsFkFwq_id(){
		return YhCache.getByFwq_id(id);
	}
	
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("mc", this.mc);
        result.put("kfsj", this.kfsj);
        result.put("gfsj", this.gfsj);
        result.put("serverkey", this.serverkey);
        result.put("sx", this.sx);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("名称", this.mc);
        result.put("开服时间", this.kfsj);
        result.put("关服时间", this.gfsj);
        result.put("serverkey", this.serverkey);
        result.put("顺序", this.sx);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.mc);
		    SK_OutputStream.writeDate(out,this.kfsj);
		    SK_OutputStream.writeDate(out,this.gfsj);
		    SK_OutputStream.writeString(out,this.serverkey);
		    SK_OutputStream.writeInt(out,this.sx);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Fwq createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Fwq fwq = new Fwq();
		    fwq.id = SK_InputStream.readInt(in,null);
		    fwq.mc = SK_InputStream.readString(in,null);
		    fwq.kfsj = SK_InputStream.readDate(in,null);
		    fwq.gfsj = SK_InputStream.readDate(in,null);
		    fwq.serverkey = SK_InputStream.readString(in,null);
		    fwq.sx = SK_InputStream.readInt(in,null);
		    return fwq;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Fwq createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Fwq fwq = Fwq.createForMap(map);
		    return fwq;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Fwq> createForColumnNameList(List<Map<String, Object>> list){
    	List<Fwq> fwqs = new ArrayList<Fwq>();
		for (Map<String, Object> map : list) {
			fwqs.add(createForColumnNameMap(map));
		}
		return fwqs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Fwq createForColumnNameMap(Map<String, Object> map){
    	Fwq obj = new Fwq();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
	    obj.kfsj = SK_Map.getDate("开服时间", map);
	    obj.gfsj = SK_Map.getDate("关服时间", map);
	    obj.serverkey = SK_Map.getString("serverkey", map);
	    obj.sx = SK_Map.getInt("顺序", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Fwq> createForList(List<Map<String, Object>> list){
    	List<Fwq> fwqs = new ArrayList<Fwq>();
		for (Map<String, Object> map : list) {
			fwqs.add(createForColumnNameMap(map));
		}
		return fwqs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Fwq createForMap(Map<String, Object> map){
    	Fwq obj = new Fwq();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("mc", map);
	    obj.kfsj = SK_Map.getDate("kfsj", map);
	    obj.gfsj = SK_Map.getDate("gfsj", map);
	    obj.serverkey = SK_Map.getString("serverkey", map);
	    obj.sx = SK_Map.getInt("sx", map);
        return obj;
    }
    
    public static Fwq createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Fwq> createForJson(List<String> jsons){
    	List<Fwq> fwqs = new ArrayList<Fwq>();
    	for(String json : jsons){
    		fwqs.add(createForJson(json));
    	}
    	return fwqs;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Fwq insert(){
    	return FwqCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Fwq update(){
    	return FwqCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return FwqCache.delete(this);
    }
    /** 即时插入数据库 */
    public Fwq insertAndFlush(){
    	return FwqCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Fwq updateAndFlush(){
    	return FwqCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return FwqCache.deleteAndFlush(this);
    }
}
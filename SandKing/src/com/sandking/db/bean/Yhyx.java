package com.sandking.db.bean;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.db.cache.YxjnCache;
import java.util.HashMap;
import com.sandking.db.cache.YhCache;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YxztCache;
import com.sandking.db.cache.YhyxCache;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 用户英雄
 */
public class Yhyx {

	public static final String TABLENAME = "用户英雄";
	public static final String CLASSNAME = "Yhyx"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 用户_id */
	private int yh_id;
	
	/** 英雄id */
	private int yxid;
	
	/** 英雄lvl */
	private int yxlvl;
	
	/** 英雄exp */
	private int yxexp;
	
	/** 英雄状态id */
	private int yxztid;
	
	/** 兵数 */
	private int bs;
	
	
	public Yhyx() {
		super();
	}
	
	public Yhyx(int id, int yh_id, int yxid, int yxlvl, int yxexp, int yxztid, int bs) {
		super();
		this.id = id;
		this.yh_id = yh_id;
		this.yxid = yxid;
		this.yxlvl = yxlvl;
		this.yxexp = yxexp;
		this.yxztid = yxztid;
		this.bs = bs;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
		addUpdateColumn("用户_id",yh_id);
	}
	
	public void changeYh_idWith(int yh_id){
		this.yh_id += yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMin(int yh_id,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMax(int yh_id,int max){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMaxMin(int yh_id,int max,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}	
	public int getYxid() {
		return yxid;
	}
	
	public void setYxid(int yxid) {
		this.yxid = yxid;
		addUpdateColumn("英雄id",yxid);
	}
	
	public void changeYxidWith(int yxid){
		this.yxid += yxid;
		setYxid(this.yxid);
	}
	
	public void changeYxidWithMin(int yxid,int min){
		this.yxid += yxid;
		this.yxid = this.yxid < min ? min : this.yxid;
		setYxid(this.yxid);
	}
	
	public void changeYxidWithMax(int yxid,int max){
		this.yxid += yxid;
		this.yxid = this.yxid > max ? max : this.yxid;
		setYxid(this.yxid);
	}
	
	public void changeYxidWithMaxMin(int yxid,int max,int min){
		this.yxid += yxid;
		this.yxid = this.yxid < min ? min : this.yxid;
		this.yxid = this.yxid > max ? max : this.yxid;
		setYxid(this.yxid);
	}	
	public int getYxlvl() {
		return yxlvl;
	}
	
	public void setYxlvl(int yxlvl) {
		this.yxlvl = yxlvl;
		addUpdateColumn("英雄lvl",yxlvl);
	}
	
	public void changeYxlvlWith(int yxlvl){
		this.yxlvl += yxlvl;
		setYxlvl(this.yxlvl);
	}
	
	public void changeYxlvlWithMin(int yxlvl,int min){
		this.yxlvl += yxlvl;
		this.yxlvl = this.yxlvl < min ? min : this.yxlvl;
		setYxlvl(this.yxlvl);
	}
	
	public void changeYxlvlWithMax(int yxlvl,int max){
		this.yxlvl += yxlvl;
		this.yxlvl = this.yxlvl > max ? max : this.yxlvl;
		setYxlvl(this.yxlvl);
	}
	
	public void changeYxlvlWithMaxMin(int yxlvl,int max,int min){
		this.yxlvl += yxlvl;
		this.yxlvl = this.yxlvl < min ? min : this.yxlvl;
		this.yxlvl = this.yxlvl > max ? max : this.yxlvl;
		setYxlvl(this.yxlvl);
	}	
	public int getYxexp() {
		return yxexp;
	}
	
	public void setYxexp(int yxexp) {
		this.yxexp = yxexp;
		addUpdateColumn("英雄exp",yxexp);
	}
	
	public void changeYxexpWith(int yxexp){
		this.yxexp += yxexp;
		setYxexp(this.yxexp);
	}
	
	public void changeYxexpWithMin(int yxexp,int min){
		this.yxexp += yxexp;
		this.yxexp = this.yxexp < min ? min : this.yxexp;
		setYxexp(this.yxexp);
	}
	
	public void changeYxexpWithMax(int yxexp,int max){
		this.yxexp += yxexp;
		this.yxexp = this.yxexp > max ? max : this.yxexp;
		setYxexp(this.yxexp);
	}
	
	public void changeYxexpWithMaxMin(int yxexp,int max,int min){
		this.yxexp += yxexp;
		this.yxexp = this.yxexp < min ? min : this.yxexp;
		this.yxexp = this.yxexp > max ? max : this.yxexp;
		setYxexp(this.yxexp);
	}	
	public int getYxztid() {
		return yxztid;
	}
	
	public void setYxztid(int yxztid) {
		this.yxztid = yxztid;
		addUpdateColumn("英雄状态id",yxztid);
	}
	
	public void changeYxztidWith(int yxztid){
		this.yxztid += yxztid;
		setYxztid(this.yxztid);
	}
	
	public void changeYxztidWithMin(int yxztid,int min){
		this.yxztid += yxztid;
		this.yxztid = this.yxztid < min ? min : this.yxztid;
		setYxztid(this.yxztid);
	}
	
	public void changeYxztidWithMax(int yxztid,int max){
		this.yxztid += yxztid;
		this.yxztid = this.yxztid > max ? max : this.yxztid;
		setYxztid(this.yxztid);
	}
	
	public void changeYxztidWithMaxMin(int yxztid,int max,int min){
		this.yxztid += yxztid;
		this.yxztid = this.yxztid < min ? min : this.yxztid;
		this.yxztid = this.yxztid > max ? max : this.yxztid;
		setYxztid(this.yxztid);
	}	
	public int getBs() {
		return bs;
	}
	
	public void setBs(int bs) {
		this.bs = bs;
		addUpdateColumn("兵数",bs);
	}
	
	public void changeBsWith(int bs){
		this.bs += bs;
		setBs(this.bs);
	}
	
	public void changeBsWithMin(int bs,int min){
		this.bs += bs;
		this.bs = this.bs < min ? min : this.bs;
		setBs(this.bs);
	}
	
	public void changeBsWithMax(int bs,int max){
		this.bs += bs;
		this.bs = this.bs > max ? max : this.bs;
		setBs(this.bs);
	}
	
	public void changeBsWithMaxMin(int bs,int max,int min){
		this.bs += bs;
		this.bs = this.bs < min ? min : this.bs;
		this.bs = this.bs > max ? max : this.bs;
		setBs(this.bs);
	}	
	
	//id
	public List<Yxjn> getYxjnsFkYhyx_id(){
		return YxjnCache.getByYhyx_id(id);
	}
	
	//用户_id
	public Yh getYhPkYh_id(){
		return YhCache.getById(yh_id);
	}
	//英雄状态id
	public Yxzt getYxztPkYxztid(){
		return YxztCache.getById(yxztid);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("yh_id", this.yh_id);
        result.put("yxid", this.yxid);
        result.put("yxlvl", this.yxlvl);
        result.put("yxexp", this.yxexp);
        result.put("yxztid", this.yxztid);
        result.put("bs", this.bs);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("用户_id", this.yh_id);
        result.put("英雄id", this.yxid);
        result.put("英雄lvl", this.yxlvl);
        result.put("英雄exp", this.yxexp);
        result.put("英雄状态id", this.yxztid);
        result.put("兵数", this.bs);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.yh_id);
		    SK_OutputStream.writeInt(out,this.yxid);
		    SK_OutputStream.writeInt(out,this.yxlvl);
		    SK_OutputStream.writeInt(out,this.yxexp);
		    SK_OutputStream.writeInt(out,this.yxztid);
		    SK_OutputStream.writeInt(out,this.bs);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Yhyx createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhyx yhyx = new Yhyx();
		    yhyx.id = SK_InputStream.readInt(in,null);
		    yhyx.yh_id = SK_InputStream.readInt(in,null);
		    yhyx.yxid = SK_InputStream.readInt(in,null);
		    yhyx.yxlvl = SK_InputStream.readInt(in,null);
		    yhyx.yxexp = SK_InputStream.readInt(in,null);
		    yhyx.yxztid = SK_InputStream.readInt(in,null);
		    yhyx.bs = SK_InputStream.readInt(in,null);
		    return yhyx;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Yhyx createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Yhyx yhyx = Yhyx.createForMap(map);
		    return yhyx;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhyx> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		for (Map<String, Object> map : list) {
			yhyxs.add(createForColumnNameMap(map));
		}
		return yhyxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhyx createForColumnNameMap(Map<String, Object> map){
    	Yhyx obj = new Yhyx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
	    obj.yxid = SK_Map.getInt("英雄id", map);
	    obj.yxlvl = SK_Map.getInt("英雄lvl", map);
	    obj.yxexp = SK_Map.getInt("英雄exp", map);
	    obj.yxztid = SK_Map.getInt("英雄状态id", map);
	    obj.bs = SK_Map.getInt("兵数", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhyx> createForList(List<Map<String, Object>> list){
    	List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		for (Map<String, Object> map : list) {
			yhyxs.add(createForColumnNameMap(map));
		}
		return yhyxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhyx createForMap(Map<String, Object> map){
    	Yhyx obj = new Yhyx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yh_id = SK_Map.getInt("yh_id", map);
	    obj.yxid = SK_Map.getInt("yxid", map);
	    obj.yxlvl = SK_Map.getInt("yxlvl", map);
	    obj.yxexp = SK_Map.getInt("yxexp", map);
	    obj.yxztid = SK_Map.getInt("yxztid", map);
	    obj.bs = SK_Map.getInt("bs", map);
        return obj;
    }
    
    public static Yhyx createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Yhyx> createForJson(List<String> jsons){
    	List<Yhyx> yhyxs = new ArrayList<Yhyx>();
    	for(String json : jsons){
    		yhyxs.add(createForJson(json));
    	}
    	return yhyxs;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Yhyx insert(){
    	return YhyxCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhyx update(){
    	return YhyxCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhyxCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhyx insertAndFlush(){
    	return YhyxCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhyx updateAndFlush(){
    	return YhyxCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhyxCache.deleteAndFlush(this);
    }
}